<?php
$PageTitle ='Sitemap';
$Add2Head = '';
include('includes/header.php');
?>

	</div><!-- header_content close -->
</div><!--Header close-->

	<div id="main_wrap">
		<div id="main_content">
			<h2>Sitemap</h2>
			<ul id="sitemap">
				<li><a href="<?php echo $baseURL?>" >Home</a>
					<ul>
						<li><a href="events.html">Community Events</a></li>
						<li><a href="includes/contact.html">Contact</a></li>
						<li><a href="includes/community_links.html">Community Links</a></li>
						<li><a href="includes/city_services.html">City Services</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $baseURL?>living/" >Living</a>
					<ul>
						<li><a href="<?php echo $baseURL?>living/education.html">Education</a></li>
						<li><a href="<?php echo $baseURL?>living/recreation.html">Recreation</a></li>
						<li><a href="<?php echo $baseURL?>living/senior_citizens_center.html">Senior Citizens Center</a></li>
						<li><a href="<?php echo $baseURL?>living/real_estate.html">Real Estate</a></li>
						<li><a href="<?php echo $baseURL?>includes/permits.html">Permits & Applications</a></li>
						<li><a href="<?php echo $baseURL?>includes/city_services.html">City Services</a></li>
						<li><a href="<?php echo $baseURL?>includes/codes.html">Codes & Ordinances</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $baseURL?>doing_business/" >Doing Business</a>
					<ul>
						<li><a href="<?php echo $baseURL?>doing_business/economic_development.html">Economic Development</a></li>
						<li><a href="<?php echo $baseURL?>doing_business/industrial_property.html">Industrial Property</a></li>
						<li><a href="<?php echo $baseURL?>doing_business/locations.html">Locations</a></li>
						<li><a href="<?php echo $baseURL?>doing_business/workforce.html">Workforce</a></li>
						<li><a href="<?php echo $baseURL?>includes/codes.html">Codes & Ordinances</a></li>
						<li><a href="<?php echo $baseURL?>includes/permits.html">Permits & Applications</a></li>
						<li><a href="<?php echo $baseURL?>doing_business/available_programs.html">Available Programs</a></li>
						<li><a href="http://wabashcountychamber.com">Chamber of Commerce</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $baseURL?>about_mount_carmel/">About Mount Carmel</a>
					<ul>
						<li><a href="<?php echo $baseURL?>includes/contact.html">Contact Information</a></li>
						<li><a href="http://www2.illinoisbiz.biz/communityprofiles/profiles/MOUNTCARMEL.htm">Local Profile</a></li>
						<li><a href="http://water.weather.gov/ahps2/hydrograph.php?wfo=ind&gage=mcri2&view=1,1,1,1,1,1,1,1%22">River Level</a></li>
						<li><a href="<?php echo $baseURL?>about_mount_carmel/history.html">History</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $baseURL?>visit/">Visit</a>
					<ul>
						<li><a href="<?php echo $baseURL?>living/recreation.html">Recreation</a></li>
						<li><a href="<?php echo $baseURL?>visit/attractions.html">Attractions</a></li>
						<li><a href="<?php echo $baseURL?>visit/road_to_mount_carmel.html">Road to Mt. Carmel</a></li>
						<li><a href="<?php echo $baseURL?>includes/codes.html">Codes & Ordinances</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $baseURL?>government/">Government</a>
					<ul>
						<li><a href="<?php echo $baseURL?>government/organizations.html">Organizations</a></li>
						<li><a href="<?php echo $baseURL?>government/city_council.html">City Council</a></li>
						<li><a href="<?php echo $baseURL?>government/foia.html">FOIA</a></li>
						<li><a href="<?php echo $baseURL?>includes/permits.html">Permits & Applications</a></li>
						<li><a href="<?php echo $baseURL?>living/senior_citizens_center.html">Senior Citizens Center</a></li>
						<li><a href="<?php echo $baseURL?>includes/city_services.html">City Services</a></li>
						<li><a href="<?php echo $baseURL?>includes/codes.html">Codes & Ordinances</a></li>
					</ul>
				</li>
			</ul>

		</div>
	</div><!--Main Wrap close-->
	
	
	
	
<?php
include('includes/footer.php');
?>