<?php
include('config.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title><?if(isset($PageTitle)){echo $PageTitle; }else{echo 'City of Mount Carmel :: Mount Carmel, Illinois ';}?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="The City of Mount Carmel, Illinois is located on the wabash river in southern Illinois. Mount Carmel offers a great community live, raise a family, go to school and work. Located in Wabash County, IL, Mount Carmel is centrally located in the midwest." />
	<meta name="keywords" content="Mount Carmel, Illinois, southern Illinois, wabash river, wabash county, wvc, aces football, the lone ranger, ag days festival, ribber fest " />
	<?if(isset($Add2Head)){echo $Add2Head;}?>
	<link type="text/css" href="<?=SITE_URL?>/assets/css/1140.css" rel="stylesheet" />
	<link type="text/css" href="<?=SITE_URL?>/assets/css/style.css" rel="stylesheet" />
	<link type="text/css" href="<?=SITE_URL?>/assets/css/layout.css" rel="stylesheet" />
	<link type="text/css" href="<?=SITE_URL?>/assets/css/contact_form.css" rel="stylesheet" />
	<link type="text/css" href="<?=SITE_URL?>/assets/css/validationEngine.jquery.css" rel="stylesheet" />
	<link type="text/css" href="<?=SITE_URL?>/assets/css/flexslider.css" rel="stylesheet" />
	<!--[if IE 7]>
	<link type="text/css" href="../css/ie.css" rel="stylesheet" />
	<![endif]-->
	<!-- Javascript / Jquery -->
		<!-- Core Files -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/jquery-1.7.js"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
		<!-- Image Wraping -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/jquery.slickwrap.min.js"></script>
		<!-- Slideshow -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/slideshow.js"></script>
		<!-- Dropdown -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/dropdown-menu.js"></script>
		<!-- Form Validation -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/languages/jquery.validationEngine-en.js"></script>
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/jquery.validationEngine.js"></script>
		<!-- idTabs -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/jquery.idTabs.min.js"></script>
		<!-- Marquee -->
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/marquee.js"></script>
			<script type="text/javascript" src="<?=SITE_URL?>/assets/js/modernizr.custom.88004.js"></script>
		<!-- Instantiation -->
			<script>
				$(document).ready(function(){
				mainmenu();
				});
			</script>
			<script type="text/javascript">
			            $(document).ready(function(){
			                $('.wrapReady').slickWrap();
			            });
			</script>
			<script>
	    		$(document).ready(function(){
	        	$("#contact_form").validationEngine('attach');
	       		});
	       </script>
	       <script>
	       	$(document).ready(function(){
		       	$("#slideshow > div:gt(0)").hide();

				setInterval(function() {
				  $('#slideshow > div:first')
				    .fadeOut(1000)
				    .next()
				    .fadeIn(1000)
				    .end()
				    .appendTo('#slideshow');
				},  5000);
				});
			</script>

	<!-- Search -->

	    <!-- Google Analytics -->
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-18661947-19']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

</head>
<body>

<div id="page_wrapper">
	<header>
		<div id="logo_wrap">
		<div id="head_right">
				<h5>Mount Carmel, Illinois</h5>

				<!--
<form method="get" action="/search">
      			    <div>
        		    	<input id="search" type= "text" name=     "q" value="" placeholder="search..."   />
        		    	<input type="hidden" name=   "cat" value=""  />
        		    	<input type="hidden" name=  "time" value="0" />
        		    	<input type="hidden" name="letter" value=""  />
        		    	<input type="hidden" name="offset" value="0" />
        		    	<input type="hidden" name=  "type" value="quick"  />
        		    	<input id="submit" type="submit"               value="Search" />
      			    </div>
      			</form>
-->
			</div>
			<a href="<?=SITE_URL?>"><img id="logo" src="<?=SITE_URL?>/assets/images/mtc_logo.png" alt="logo" /></a>

		</div>
		<div id="header_content">
		<a class="open" href="#menu">Menu</a>
		<nav>
			<ul class="main group" id="menu">
				<li id="home"><a href="<?=SITE_URL?>" >Home</a></li>
				<li id="living"><em><a href="#">Residence</a></em>
					<ul id="ResideceDropDown">
						<li><a href="<?=SITE_URL?>/education.html">Education</a></li>
						<li><a href="<?=SITE_URL?>/recreation.html">Recreation</a></li>
						<li><a href="<?=SITE_URL?>/senior-citizens-center.html">Senior Citizens Center</a></li>
						<li><a href="<?=SITE_URL?>/real-estate.html">Real Estate</a></li>
						<li><a href="<?=SITE_URL?>/forms.html">Forms</a></li>
						<li><a href="<?=SITE_URL?>/city-services.html">City Services</a></li>
						<li><a href="<?=SITE_URL?>/codes.html">Codes &amp;  Ordinances</a></li>
						<li><a href="<?=SITE_URL?>/cemetery-records.html">Cemetery Records</a></li>
					</ul>
				</li>
				<li id="business"><em><a href="#"><span>Doing</span> Business</a></em>
					<ul id="DoingBusinessDropDown">
						<li><a href="<?=SITE_URL?>/economic-development.html">Economic Development</a></li>
						<li><a href="<?=SITE_URL?>/industrial-property.html">Industrial Property</a></li>
						<li><a href="<?=SITE_URL?>/locations.html">Location</a></li>
						<li><a href="<?=SITE_URL?>/workforce.html">Workforce</a></li>
						<li><a href="<?=SITE_URL?>/codes.html">Codes &amp; Ordinances</a></li>
						<li><a href="<?=SITE_URL?>/forms.html">Forms</a></li>
						<li><a href="<?=SITE_URL?>/available-programs.html">Available Programs</a></li>
						<li><a href="http://wabashcountychamber.com" target="_blank">Chamber of Commerce</a></li>
						<li><a href="http://mcaea.com/" target="_blank">Area Economic Alliance </a></li>
					</ul>
				</li>
				<li id="about"><em><a href="#">About <span>Mount Carmel</span></a></em>
					<ul id="AboutMountCarmelDropDown">
						<li><a href="<?=SITE_URL?>/contact.html">Contact Information</a></li>
						<li><a href="http://www2.illinoisbiz.biz/communityprofiles/profiles/MOUNTCARMEL.htm" target="_blank">Local Profile</a></li>
						<li><a href="http://water.weather.gov/ahps2/hydrograph.php?wfo=ind&gage=mcri2&view=1,1,1,1,1,1,1,1%22" target="_blank">River Level</a></li>
						<!-- <li><a href="<?php //echo $baseURL?>about_mount_carmel/history.html">History</a></li> -->
					</ul>
				</li>
				<li id="visit"><em><a href="#">Visit</a></em>
					<ul id="VisitDropDown">
						<li><a href="<?=SITE_URL?>/recreation.html">Recreation</a></li>
						<li><a href="<?=SITE_URL?>/attractions.html">Attractions</a></li>
						<li><a href="<?=SITE_URL?>/road-to-mount-carmel.html">Road to Mt. Carmel</a></li>
						<li><a href="<?=SITE_URL?>/hotels.html">Hotels</a></li>
						<li><a href="<?=SITE_URL?>/restaurants.html">Restaurants</a></li>
						<li><a href="<?=SITE_URL?>/forms.html">Forms</a></li>
					</ul>
				</li>
				<li id="government"><em><a href="#">Government</a></em>
					<ul id="GovernmentDropDown">
						<li><a href="<?=SITE_URL?>/organization.html">Organization</a></li>
						<li><a href="<?=SITE_URL?>/city-council-meetings.html">City Council Meetings</a></li>
						<li><a href="<?=SITE_URL?>/foia.html">FOIA</a></li>
						<li><a href="<?=SITE_URL?>/forms.html">Forms</a></li>
						<li><a href="<?=SITE_URL?>/senior-citizens-center.html">Senior Citizens Center</a></li>
						<li><a href="<?=SITE_URL?>/city-services.html">City Services</a></li>
						<li><a href="<?=SITE_URL?>/codes.html">Codes &amp; Ordinances</a></li>
						<li><a href="<?=SITE_URL?>/cemetery-records.html">Cemetery Records</a></li>
						<li><a href="<?=SITE_URL?>/police.html">Police</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		</div><!-- header_content close -->
</div><!--Header close-->

