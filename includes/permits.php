<?php
$PageTitle ='Permits &amp; Applications';

include('../includes/header.php');

?>

	</div><!-- header_content close -->
</div><!--Header close-->

	<div id="main_wrap">
		<div id="main_content">
			<h2>Permits &amp; Applications</h2><br />
			<div class="cushycms" id="permits"><br />
<h4>
	Permits</h4>
<table>
	<tbody>
		<tr class="pdfs">
			<td>
				Building Permit</td>
			<td>
				<a href="../white-pages/Building_Permit_City_Of_Mt_Carmel.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Coin Operated Device Permit</td>
			<td>
				<a href="../white-pages/COD_LICENSE_APPLICATION.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Flood Plain Development Permit</td>
			<td>
				<a href="../white-pages/Flood_Plain_Dev_Permit.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<!--
<tr class="pdfs">
					<a href="../white-pages/Mount%20Carmel/RV%20PERMIT.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a></td>
					<td>RV Permit</td>
				</tr>
-->
		<tr class="pdfs">
			<td>
				Special Use Permit</td>
			<td>
				<a href="../white-pages/SPECIAL_USE_PERMIT_APPLICATION.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr>
			<td>
				Contractor Registration</td>
			<td>
				<a href="http://cityofmtcarmel.com/minutes/permits/ContractorRegistration.pdf" target="_blank"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr>
			<td>
				Sign Application</td>
			<td>
				<a href="http://cityofmtcarmel.com/minutes/permits/SIGNS APPENDIX H.pdf" target="_blank"><img alt="" src="/images/permits_19_2252023846.png" style="width: 30px; height: 30px;" /></a></td>
		</tr>
		<tr>
			<td>
				<a href="/images/permits_23_83095044.pdf" target="_blank">Video Gaming Application</a></td>
			<td>
				&nbsp;</td>
		</tr>
	</tbody>
</table>
</div>
			<div class="cushycms" id="applications"><h4>
	Applications</h4>
<table>
	<tbody>
		<tr class="pdfs">
			<td>
				Birth Record Search Application</td>
			<td>
				<a href="../white-pages/Birth_Record_Search_Application.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Building Permit Application</td>
			<td>
				<a href="../white-pages/Building_Permit_City_Of_Mt_Carmel.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Death Record Search Application</td>
			<td>
				<a href="../white-pages/Death_Record_Search_Application.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Demolition Ordinance</td>
			<td>
				<a href="http://cityofmtcarmel.com/minutes/permits/DemolitionOrdinance.pdf" target="_blank"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" style="width: 30px; height: 30px;" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Demo Permit Application A</td>
			<td>
				<a href="../white-pages/Demo_Application_A.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Demo Permit Application B</td>
			<td>
				<a href="../white-pages/Demo_Application_B.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Elevation Certificate M</td>
			<td>
				<a href="http://www.fema.gov/pdf/nfip/elvcert.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Fence Application</td>
			<td>
				<a href="../white-pages/Fence_Application.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Liquor License Application</td>
			<td>
				<a href="../white-pages/LIQUOR_LICENSE_application.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<!--
				<tr class="pdfs">
					<a href="../white-pages/Raffle_Lic_Application-11-10.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
					<td>Raffle License Application</td>
				</tr>
				--><!--
				<tr class="pdfs">
					<a href="../white-pages/SIGNS_APPENDIX_H.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
					<td>Sign Application</td>
				</tr>
				--><!--
				<tr class="pdfs">	
					<a href="../white-pages/Solicitors_Peddlers_Application.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
					<td>Solicitor Peddler Application</td>
				</tr>
				-->
		<tr class="pdfs">
			<td>
				Subdivision Application B</td>
			<td>
				<a href="../white-pages/SUBDIVISION_APPLICATION_TYPE_B.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Swimming Pool Application</td>
			<td>
				<a href="../white-pages/SWIMMING_POOL_PERMIT_APPLICATION.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Variance Application</td>
			<td>
				<a href="../white-pages/REQUEST_VARIANCE_APPLICATION.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Water Service Application</td>
			<td>
				<a href="../white-pages/WATER_SERVICE_APP2.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Water Application Instructions</td>
			<td>
				<a href="../white-pages/WATER_APPLICATION_INSTRUCTIONS.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Water Tap City Limits Application</td>
			<td>
				<a href="../white-pages/Water_Tap_City_Limits_Application.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Water Tap Outside City Application</td>
			<td>
				<a href="../white-pages/Water_Tap_Application_Outside_City.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
		<tr class="pdfs">
			<td>
				Water Termination Application</td>
			<td>
				<a href="../white-pages/WATER_SERVICE_termination.pdf"><img alt="pdf-icon" src="../images/icons/pdf-icon.png" /></a></td>
		</tr>
	</tbody>
</table>
<p>
	&nbsp;</p>
</div>
		<div class="clear h_trider"></tr>
		<img src="../images/building-plans.jpg" alt="building plans" class="right" />
		<div class="cushycms"><h3>
	When is a building permit required?</h3>
<ol>
	<li>
		Construction of any building or other structure with a total construction cost in excess of $100.00.</li>
	<li>
		Construction of an addition to any building or structure.</li>
	<li>
		Demolition of a building or structure.</li>
	<li>
		A change in the occupancy of a building that effects fire egress and exit requirements or other occupancy restrictions.</li>
	<li>
		The installation or alteration of any equipment regulated by this code.</li>
	<li>
		Any change to a roof line, e.g. from a flat roof to a gabled or hip roof also normal shingle replacement.</li>
</ol>
<p>
	Electrical permits, plumbing permits and mechanical permits are considered separately.<br />
	A building permit is NOT required for any building maintenance, such as siding, painting, replacement of doors and windows in existing openings. UNLESS, it would cause a change in occupancy requirements or affect required fire egress and exits etc.</p>
</div>
		<div class="cushycms"><h3>
	What is the procedure for Permit Application?</h3>
<ol>
	<li>
		<a href="../white-pages/Building_Permit_City_Of_Mt_Carmel.pdf">Fill out the Application for Building Permit</a> and Plan Review, providing all information required for the project. For one and two family dwellings the applicant must also complete the One &amp; Two Family Plan Review form.
		<ul>
			<li>
				For structures to be constructed within a subdivision, the applicant should be aware of any restrictive covenants that may apply. Generally, covenants are recorded at the Office of the County Clerk.</li>
		</ul>
	</li>
	<li>
		Any application submitted for the construction of a building or structure to be used by the public MUST include a set of plans for such on which an architect or structural engineer licensed by the State of Illinois to perform such work has signed and affixed his seal thereon.</li>
	<li>
		The site plan must indicate all set-back distances and dimensions (check for conformance with zoning regulations). All set-backs are measured from the property line and NOT from the curb or street center.</li>
	<li>
		A requirement for the issuance of a demolition permit is for the applicant to NOTIFY JULIE for a dig number. This will assure that utilities have been properly disconnected.</li>
</ol>
</div>


		</div>
	</div><!--Main Wrap close-->
	
	
	
	
<?php
include('../includes/footer.php');
?>