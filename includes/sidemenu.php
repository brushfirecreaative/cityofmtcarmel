<ul class="side" id="SideMenu">
	<li style="text-align: center">
		<h3>
			Quick Links</h3>
	</li>
	<li>
		<a href="http://wabashcountychamber.com/events.htm" target="_blank">Community Events</a></li>
	<li>
		<a href="/contact.html">Contact</a></li>
	<li>
		<a href="<?=SITE_URL?>/community-links.html">Community Links</a></li>
	<li>
		<a href="<?=SITE_URL?>/city-services.html">City Services</a></li>
	<li>
		<a href="/road-to-mount-carmel.html">Road to Mt Carmel</a></li>
	<li>
		<a href="http://wabashcountychamber.com" target="_blank">Chamber of Commerce </a></li>
	<li>
		<a href="http://www.mcaea.com/">Area Economic Alliance </a></li>
</ul>
