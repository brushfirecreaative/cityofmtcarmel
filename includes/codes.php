<?php
$PageTitle ='Codes and Ordinances';



include('../includes/header.php');


//========
//LICENSE
//========

//The CSS, XHTML, PHP and design is released under Creative Commons Attribution v2.5:
//http://creativecommons.org/licenses/by/2.5/

//FormCheck (http://moo.floor.ch/?p=18) is released under the MIT license:
//http://www.opensource.org/licenses/mit-license.php

//==============
//CONFIGURATION
//==============

//IMPORTANT!!
//Put in your email address below:
$to = 'mgidcumb@cityofmtcarmel.com';


//User info (DO NOT EDIT!)
$name = stripslashes($_POST['name']); //sender's name
$email = stripslashes($_POST['email']); //sender's email

//The subject
$subject  = "Website Inquiry: "; //The default subject. Will appear by default in all messages. Change this if you want.
$subject .= stripslashes($_POST['subject']); // the subject


//The message you will receive in your mailbox
//Each parts are commented to help you understand what it does exaclty.
//YOU DON'T NEED TO EDIT IT BELOW BUT IF YOU DO, DO IT WITH CAUTION!
$msg  = "From : $name \r\n";  //add sender's name to the message
$msg .= "e-Mail : $email \r\n";  //add sender's email to the message
$msg .= "Subject : $subject \r\n\n"; //add subject to the message (optional! It will be displayed in the header anyway)
$msg .= "---Message--- \r\n".stripslashes($_POST['message'])."\r\n\n";  //the message itself


?>

	</div><!-- header_content close -->
</div><!--Header close-->

	<div id="main_wrap">
		<div id="main_content">
			<h2>Codes and Ordinances</h2>
			<img class="right cushycms" src="../images/roofers.jpg" alt="roofing" />
						<br />
			<div class="cushycms"><div class="pdfs">
	<a href="../white-pages/Mount_Carmel_Code_Enforcement_Brochure.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Code Enforcemnt Brochure</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Arc-Fault_Circuit_Interrupter.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Arc-Fault Circuit Interrupter</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Smoke_Alarm_Regulations.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Smoke Alarm Regulations</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Illinois_Roofing_Licensing_Act.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Illinois Roofers and Licensing Act</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Illinois_Roofers_License_Exam.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Illinois Roofers License Exam</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Illinois_Architecture_Practice_Act.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Illinois Architecture Practice Act</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Home%204/Permits/Fence%20Ordinance.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Fence Ordinance</p>
</div>
<div class="pdfs">
	<a href="../white-pages/Home%204/Permits/Flood%20Plain%20Variance.pdf"><img src="../images/icons/pdf-icon.png" alt="pdf-icon" /></a>
	<p>
		Flood Plain Variance</p>
</div>
<p>
	<a href="http://library3.municode.com/default-test/home.htm?infobase=14746&amp;doc_action=whatsnew" target="_blank">Full List of Codes &amp; Ordanances</a></p>
</div>
				<br /><br />
				<div class="clear h_divider"></div>

			<p class="cushycms">City Inspector Michael Gidcumb is charged with the enforcement of all Mount Carmel city codes. His office is located in City Hall, 219 Market Street.</p>
			<div class="contact_form">
		
				<!-- The contact form starts here-->
				<?php
				   if ($_SERVER['REQUEST_METHOD'] != 'POST'){
				      $self = $_SERVER['PHP_SELF'];
				?>
				
				    <!-- Start HTML form -->
				   	<form name="form" method="post" id="contact_form" action="<?php echo $self;?>"  class="niceform">
				        <label for="name">
				        <strong><span class="yellow">*</span>Name :</strong>
				        </label>
				        <div>
							<img class="inputCorner" src="../images/contact_images/input_left.gif">
							<input id="name" class="textinput validate[required]" type="text" size="20" name="name" style="width: 200px;">
							<img class="inputCorner" src="../images/contact_images/input_right.gif">
						</div>
						<label for="email">
						<strong>
						<span class="yellow">*</span>
						Email :
						</strong>
						</label>
						<img class="inputCorner" src="../images/contact_images/input_left.gif">
						<input id="email" class="textinput validate[required,custom[email]]" type="text" size="20" name="email" style="width: 200px;">
						<img class="inputCorner" src="../images/contact_images/input_right.gif">
						<label for="subject">
						<strong>
						<span class="yellow">*</span>
						Subject :
						</strong>
						</label>
						<img class="inputCorner" src="../images/contact_images/input_left.gif">
						<input id="subject" class="textinput validate[required]" type="text" size="20" name="subject" style="width: 200px;">
						<img class="inputCorner" src="../images/contact_images/input_right.gif">
						<div id="message_box">
						<label for="msg">
						<strong>
						<span class="yellow">*</span>
						Your message :
						</strong>
						</label>
						<br>
						<div class="txtarea" style="width: 320px; height: 120px;">
						<div class="tr">
						<img class="txt_corner" src="../images/contact_images/txtarea_tl.gif">
						</div>
						<div class="cntr">
						<div class="cntr_l" style="height: 110px;"></div>
						<textarea id="message" class="validate[required]" cols="30" rows="10" type="text" name="message" style="width: 300px; height: 100px;">
						</textarea>
						</div>
						<div class="br">
						<img class="txt_corner" src="../images/contact_images/txtarea_bl.gif">
						</div>
						</div>
						</div>
						<label for="spamcheck">
						<span class="yellow">*</span>
						<acronym title="[ Spam prevention ]">
						<strong>Are you human?</strong>
						</acronym>
						<strong>
						:
						<br>
						<br>
						<span class="yellow">2 + 3 = ???</span>
						</strong>
						</label>
						<img class="inputCorner" src="../images/contact_images/input_left.gif">
						<input id="spamcheck" class="textinput validate[required,custom[spamCheck]]" type="text" size="5" name="spamcheck" style="width: 50px;">
						<img class="inputCorner" src="../images/contact_images/input_right.gif">
						<br>
						<br>
						<img class="buttonImg" src="../images/contact_images/button_left.gif">
						<input class="buttonSubmit" type="submit" value="Send it!">
						<img class="buttonImg" src="../images/contact_images/button_right.gif">
						<div id="stylesheetTest"></div>
				<?php
				} else {
			        error_reporting(0);
			
			      	if  (mail($to, $subject, $msg, "From: $email\r\nReply-To: $email\r\nReturn-Path: $email\r\n"))
			
				  	//Message sent!
				  	//It the message that will be displayed when the user click the sumbit button
				  	//You can modify the text if you want
			      	echo nl2br("
				   	<div class=\"MsgSent\">
						<h1>Congratulations!!</h1>
						<p>Thank you <b>$name</b>, your message is sent!<br /> someone will get back to you as soon as possible.</p>
					</div>
				   ");
			
			       	else
			
				    // Display error message if the message failed to send
			        echo "
				   	<div class=\"MsgError\">
						<h1>Error!!</h1>
						<p>Sorry <b>$name</b>, your message failed to send. Try later!</p>
					</div>";
				}
			?>
				    </form>
			
			</div>
			<div class="cushycms"><h3>
	City Inspector</h3>
<div class="contact">
	<h4>
		<i>Michael Gidcumb</i></h4>
	<div class="address">
		&nbsp;</div>
	<p>
		219 N. Market St.</p>
	<div class="phone">
		&nbsp;</div>
	<p>
		(618) 262-4822</p>
	<div class="fax">
		&nbsp;</div>
	<p>
		(618) 262-4802</p>
	<div class="email">
		&nbsp;</div>
	<p>
		<a href="mailto:mgidcumb@cityofmountcarmel.com">mgidcumb@cityofmountcarmel.com</a></p>
	<div class="clear">
		&nbsp;</div>
</div>
</div>
		</div>
	</div><!--Main Wrap close-->
	
	
	
	
<?php
include('../includes/footer.php');
?>