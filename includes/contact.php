<?php
$PageTitle ='Contact Information';
$Add2Head = '<style> h5{padding-top:0;}</style>';
include('header.php');

//========
//LICENSE
//========

//The CSS, XHTML, PHP and design is released under Creative Commons Attribution v2.5:
//http://creativecommons.org/licenses/by/2.5/

//FormCheck (http://moo.floor.ch/?p=18) is released under the MIT license:
//http://www.opensource.org/licenses/mit-license.php

//==============
//CONFIGURATION
//==============

//IMPORTANT!!
//Put in your email address below:
$to = 'mgidcumb@cityofmtcarmel.com';


//User info (DO NOT EDIT!)
$name = stripslashes($_POST['name']); //sender's name
$email = stripslashes($_POST['email']); //sender's email

//The subject
$subject  = "Website Inquiry: "; //The default subject. Will appear by default in all messages. Change this if you want.
$subject .= stripslashes($_POST['subject']); // the subject


//The message you will receive in your mailbox
//Each parts are commented to help you understand what it does exaclty.
//YOU DON'T NEED TO EDIT IT BELOW BUT IF YOU DO, DO IT WITH CAUTION!
$msg  = "From : $name \r\n";  //add sender's name to the message
$msg .= "e-Mail : $email \r\n";  //add sender's email to the message
$msg .= "Subject : $subject \r\n\n"; //add subject to the message (optional! It will be displayed in the header anyway)
$msg .= "---Message--- \r\n".stripslashes($_POST['message'])."\r\n\n";  //the message itself



?>

	</div><!-- header_content close -->
</header>

	<div id="main_wrap">
		<section id="main_content">
			<div class="row">
				<h2 class="cushycms" id="PageHeading">Contact Information</h2>
				<p class="cushycms">Mount Carmel's Public Works departments manage the city's transportation systems, levee systems, parks, recreation, cemeteries,  solid waste management services,  storm water, drinking water, and sewer systems to enhance the quality of life for the Mount Carmel community.</p>
				
				<div class="contact_form">
					<h4>General Contact Form</h4>
					<!-- The contact form starts here-->
					<?php
					   if ($_SERVER['REQUEST_METHOD'] != 'POST'){
					      $self = $_SERVER['PHP_SELF'];
					?>
					
					    <!-- Start HTML form -->
					   	<form name="form" method="post" id="contact_form" action="<?php echo $self;?>"  class="niceform">
					        <label for="name">
					        <strong><span class="yellow">*</span>Name :</strong>
					        </label>
					        <div>
								<img class="inputCorner" src="../images/contact_images/input_left.gif">
								<input id="name" class="textinput validate[required]" type="text" size="20" name="name" style="width: 200px;">
								<img class="inputCorner" src="../images/contact_images/input_right.gif">
							</div>
							<label for="email">
							<strong>
							<span class="yellow">*</span>
							Email :
							</strong>
							</label>
							<img class="inputCorner" src="../images/contact_images/input_left.gif">
							<input id="email" class="textinput validate[required,custom[email]]" type="text" size="20" name="email" style="width: 200px;">
							<img class="inputCorner" src="../images/contact_images/input_right.gif">
							<label for="subject">
							<strong>
							<span class="yellow">*</span>
							Subject :
							</strong>
							</label>
							<img class="inputCorner" src="../images/contact_images/input_left.gif">
							<input id="subject" class="textinput validate[required]" type="text" size="20" name="subject" style="width: 200px;">
							<img class="inputCorner" src="../images/contact_images/input_right.gif">
							<div id="message_box">
							<label for="msg">
							<strong>
							<span class="yellow">*</span>
							Your message :
							</strong>
							</label>
							<br>
							<div class="txtarea" style="width: 100%; height: 120px;">
							<div class="tr">
							<img class="txt_corner" src="../images/contact_images/txtarea_tl.gif">
							</div>
							<div class="cntr">
							<div class="cntr_l" style="height: 110px;"></div>
							<textarea id="message" class="validate[required]" cols="30" rows="10" type="text" name="message" style="width: 300px; height: 100px;">
							</textarea>
							</div>
							<div class="br">
							<img class="txt_corner" src="../images/contact_images/txtarea_bl.gif">
							</div>
							</div>
							</div>
							<label for="spamcheck">
							<span class="yellow">*</span>
							<acronym title="[ Spam prevention ]">
							<strong>Are you human?</strong>
							</acronym>
							<strong>
							:
							<br>
							<br>
							<span class="yellow">2 + 3 = ???</span>
							</strong>
							</label>
							<img class="inputCorner" src="../images/contact_images/input_left.gif">
							<input id="spamcheck" class="textinput validate[required,custom[spamCheck]]" type="text" size="5" name="spamcheck" style="width: 50px;">
							<img class="inputCorner" src="../images/contact_images/input_right.gif">
							<br>
							<br>
							<img class="buttonImg" src="../images/contact_images/button_left.gif">
							<input class="buttonSubmit" type="submit" value="Send it!">
							<img class="buttonImg" src="../images/contact_images/button_right.gif">
							<div id="stylesheetTest"></div>
					<?php
					} else {
				        error_reporting(0);
				
				      	if  (mail($to, $subject, $msg, "From: $email\r\nReply-To: $email\r\nReturn-Path: $email\r\n"))
								{
					  	//Message sent!
					  	//It the message that will be displayed when the user click the sumbit button
					  	//You can modify the text if you want
				      	echo nl2br("
					   	<div class=\"MsgSent\">
							<h1>Congratulations!!</h1>
							<p>Thank you <b>$name</b>, your message is sent!<br /> someone will get back to you as soon as possible.</p>
						</div>
					   ");
							}
				       	else{
				
					    // Display error message if the message failed to send
				        echo "
					   	<div class=\"MsgError\">
							<h1>Error!!</h1>
							<p>Sorry <b>$name</b>, your message failed to send. Try later!</p>
						</div>";
					}
				}
				?>
					    </form>
				
				</div>
	
				<div class="cushycms" id="DeparmentsContactInfo">
				<h4>Departments:</h4>
					<table>
						<tr>
							<td><h5>Airport:</h5></td>
							<td>618-948-2413</td>
						</tr>
						<tr>
							<td><h5>City Hall:</h5></td>
							<td>618-262-4822</td>
						</tr>
						<tr>
							<td><h5>City Hall Fax:</h5></td>
							<td>618-262-4208</td>
						</tr>
						<tr>
							<td><h5>Fire:</h5></td>
							<td>618-262-4311</td>
						</tr>
						<tr>
							<td><h5>Golf Course:</h5></td>
							<td>618-262-5771</td>
						</tr>
						<tr>
							<td><h5>Maintenance:</h5></td>
							<td>618-262-2099</td>
						</tr>
						<tr>
							<td><h5>Police:</h5></td>
							<td>618-262-4114</td>
						</tr>
						<tr>
							<td><h5>Senior Citizens:</h5></td>
							<td>618-262-7403</td>
						</tr>
						<tr>
							<td><h5>Sewer Plant:</h5></td>
							<td>618-263-3422</td>
						</tr>
						<tr>
							<td><h5>Streets &amp; Parks:</h5></td>
							<td>618-262-2358</td>
						</tr>
						<tr>
							<td><h5>Swimming Pool:</h5></td>
							<td>618-262-4042</td>
						</tr>
						<tr>
							<td><h5>Water Billing:</h5></td>
							<td>618-262-7461</td>
						</tr>
						<tr>
							<td><h5>Water Plant:</h5></td>
							<td>618-262-4871</td>
						</tr>
					</table>
					<br>
						<h4>Address</h4>
						Mt. Carmel City Hall<br>
						219 N Market St<br>
						Mt. Carmel, IL 62863

						<h4><br>Email Links</h4>
						<a href="mailto:rudyw@cityofmtcarmel.com">Rudy Witsman - City Clerk</a> <br>
						<a href="mailto:mgidcumb@cityofmtcarmel.com">Webmaster</a>
				</div>
			</div>
		</div>
	</section><!--Main Wrap close-->
	
	
	
	
<?php
include('footer.php');
?>