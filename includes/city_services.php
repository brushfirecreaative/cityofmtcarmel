<?php
$PageTitle ='Mount Carmel City Services';
$Add2Head = '';
include('../includes/header.php');
?>

	</div><!-- header_content close -->
</div><!--Header close-->

	<div id="main_wrap">
		<div id="main_content">
			<h2>City Services</h2>
			<ul class="idTabs">
						<li><a href="#water">Water &amp; Sewage</a></li>
						<li><a href="#garbage">Garbage</a></li>
						<li><a href="#apps">Applications &amp; Regulations</a></li>
					</ul>
					<div class="clear spacer"></div>
			<div id="water">
				<img class="right cushycms" src="../images/WaterFaucet.jpg" alt="water" width="300" />
				<div class="cushycms"><p>
	The City of Mount Carmel provides water and sewer service throughout the city and other limited areas. Listed below are rate schedules for water and sewer usage.</p>
<h4>
	Water Rate</h4>
<small>Effective after 04/30/2011</small>
<table>
	<tbody>
		<tr>
			<td class="table_head">
				Usage</td>
			<td class="table_head">
				Rate</td>
		</tr>
		<tr>
			<td>
				Minimum (First 1,000 gallons)</td>
			<td>
				$7.09</td>
		</tr>
		<tr>
			<td>
				Next 99,000 gallons</td>
			<td>
				$4.71</td>
		</tr>
		<tr>
			<td>
				Next 150,000 gallons</td>
			<td>
				$4.22</td>
		</tr>
		<tr>
			<td>
				Next 500,000 gallons</td>
			<td>
				$3.41</td>
		</tr>
		<tr>
			<td>
				Next 500,000 gallons</td>
			<td>
				$2.74</td>
		</tr>
		<tr>
			<td>
				Over 1,250,000 gallons</td>
			<td>
				$1.87</td>
		</tr>
	</tbody>
</table>
<p>
	<small>All rates are Per 1,000 gallons</small></p>
</div>
				<div class="cushycms"><h4>
	Water Tap Fees<br />
	<br />
	Inside Corporate Limits<br />
	<span style="color: #696969"><kbd><code><var>3/4&nbsp;Inch Tap&nbsp; $550.00,&nbsp;1 Inch Tap $750.00,&nbsp;2 Inch Tap $ 2,350.00, Tap Larger than 2 Inches will include additional costs</var></code></kbd></span><br />
	<br />
	Outside Corporate Limits<br />
	<span style="color: #696969"><samp>3/4 Inch Tap&nbsp; $650.00,&nbsp;1 Inch Tap $850.00,&nbsp;&nbsp; 2 Inch Tap $ 2,550.00,<br />
	Tap Larger than 2 Inches will include additional costs</samp></span><br />
	<br />
	<br />
	<br />
	Sewage Tap Fees</h4>
<p>
	Each user connecting to the sewer system is charged a sewer tap fee.</p>
<table>
	<tbody>
		<tr>
			<td>
				Inside Corporate Limits</td>
			<td>
				$350.00</td>
		</tr>
		<tr>
			<td>
				Outside Corporate Limits</td>
			<td>
				$500.00</td>
		</tr>
	</tbody>
</table>
</div>
			</div>
			<div id="garbage">
				<img class="right cushycms" src="../images/garbage.jpg" alt="garbage" width="300" style="border:none; background:none;" />
				<div class="cushycms"><h3>
	Garbage</h3>
<p>
	The City also provides garbage pick up for residential users both inside and outside the corporate limits.</p>
<table>
	<tbody>
		<tr>
			<td>
				Inside Corporate limits</td>
			<td>
				$13.00</td>
		</tr>
		<tr>
			<td>
				Outside Corporate limits</td>
			<td>
				$17.75</td>
		</tr>
	</tbody>
</table>
</div>
			</div>
			<div id="apps">
				<div class="cushycms"><h4>
	Application for service; and deposit.</h4>
<p>
	Water and sewer service must be applied for in writing to the Water and Sewer department, located in the city hall building located at 219 N. Market Street. For customers not owning legal title to the property where service is requested, a security deposit is required.</p>
<p>
	(a) Residential property where the customer does not own legal title - One Hundred Dollars ($100.00);</p>
<p>
	(b) Commercial or Industrial uses - One Hundred Twenty-Five Dollars ($125.00).</p>
<p>
	When service is discontinued, the city shall refund the deposit after deducting therefrom any amounts owed by the user for unpaid water or sewer bills. (Ord. No. 114, 2-10-64; Ord. No. 368, x 1, 2-6-78; Ord. No. 388, x 1, 11-29-78; Ord. No. 448, x 1, 11-21-83,Ord. No. 368, x 2, 2-6-78; Ord. No. 448, x 2, 11-21-8; Ord. No. 913, x, 04/21/08)</p>
</div>
				<div class="cushycms"><h4>
	When billings are due and payable; penalties.</h4>
<p>
	All payments for water and sewerage services become due and payable as follows:<br />
	Cycle 1 on the fifth (5th) day of each month<br />
	Cycle 2 on the fifteenth (15th) day of each month<br />
	Cycle 3 on the twenty-fifth (25th) day of each month</p>
<p>
	If not paid by the dates listed above, ten (10) percent will be added to the net amounts;</p>
<div class="cushycms">
	<h4>
		Discontinuation Of Service For Nonpayment - Reconnection Charge</h4>
	(a) If water and sewer service charges are not paid within seven ( 7 ) days following the due date, the customer&#39;s or user&#39;s account shall be deemed delinquent. In the event of delinquency, the city clerk shall terminate water service and the customer or user shall not again be entitled to water service until the water and sewer charges have been paid in full, together with a service reconnection charge of twenty-five dollars ($25.00).
	<div class="cushycms">
		<h4>
			Arrangement Procedure</h4>
		<p>
			(a) All current utility charges must be paid in full by the due date on bill. (i.e. Cycle 1 due by the 5th of the month; Cycle 2 due by the 15th of the month; Cycle 3 by the 25th of the month.)</p>
		<p>
			(b) Customers who are unable to meet their financial obligation for utility service resulting from temporary circumstances; such as unemployment, death in family, extended illness, or abnormally high medical expenses; are encouraged to seek appropriate financial institution or social service agency assistance in order to meet the requirements of their utility charges for the current period in order to avoid the delinquency deadline and service termination/shut-off proceedings.</p>
		<p>
			(c) Individuals must provide written evidence and the City must verify by the due date of the current billing month that arrangements have been made between the customer and a bonafide social service agency or financial institution to meet the complete financial obligation of the current and any delinquent utility charges.</p>
		<p>
			(d) Upon receipt of the written evidence and verification of payment assistance by the Utility Billing Office, the City shall extend the shut-off date for the given individual as follows: Cycle 1 shut-off date the 14th of the current billing month; Cycle 2 shut-off date the 23rd of the current billing month; Cycle 3 shut-off date the 3rd of the following month. Individuals may seek not more than two payment assistance payment extensions in a calendar year.</p>
		<p>
			(e) No extension will be granted based on personal credit or promises to pay by individual utility customers.</p>
		<p>
			(f) All payment assistance payment extensions must be placed in writing; signed by both the customer and City Service Director and filed with Utility Billing Office.</p>
		<p>
			(g) Failure of the social service agency or financial institution to remit payment within twenty calendar days will subject the customer to proceedings under the Disconnection for Non-Payment/Shut-Off.</p>
		<p>
			(h) If the social service agency or financial institution cannot guarantee payment in full within the twenty-day time limit, written notice from the institution must accompany the application for payment assistance payment extension and explicitly state when payment will be remitted to the City.</p>
		<p>
			(i) Utility services that have already been disconnected for non-payment are not eligible for payment extensions. All charges associated with said accounts must be paid in full in cash prior to service reestablishment.</p>
		<p>
			(j) Continuing requests for payment arrangements will not be accepted. Once payment arrangement options as established above are exhausted, service termination for nonpayment/shut off procedures shall result.</p>
	</div>
</div>
</div>
		</div>
	</div><!--Main Wrap close-->
	
	
	
	
<?php
include('../includes/footer.php');
?>