<?php

/* $Add2Head = '<style type="text/css">#header{height:460px !important;}#header_content{height:400px !important;}</style>'; */

include('includes/header.php');
?>

		<div class="row" id="homepage-header">
			<div class="ninecol">
				<section id="slider">
					<div id="slideshow">
						<div>
		  	    	   	 <img src="/assets/images/web-city-hall.jpg" />
		  	    		</div>
		  	    		<div>
		  	    	    	<img src="/assets/images/golf_course.jpg" />
		  	    		</div>
						<div>
		  	    	   	 <img src="/assets/images/pool.jpg" />
		  	    		</div>

		  	    		<div>
		  	    	   	 <img src="/assets/images/college_sign.jpg" />
		  	    		</div>

					</div>
			      </section>
			</div>
			<div class="threecol lastcol">
	      <?php
			include('includes/sidemenu.php');
			?>
		</div>
		</div>

	</div><!-- header_content close -->
</header><!--Header close-->

	<div id="main_wrap">
	<div id="main">
	<div class="marquee"><marquee behavior="scroll" scrollamount="5"><p id="ScrollingNewsAlert">
	<p> ....City Council Meeting  October 10  @ 5 PM.......Link to Customer Confidence Report under News Section.......Link to Online Bill Pay is under News Section
</p>
</p></marquee></div>
		<!-- Spotlight -->
		<section id="city_update">
			<h1>City <span>Updates</span></h1>
			<div class="row">
				<div class="fourcol">
					<h3>In the Spotlight</h3>
					<div class="spotlight_content">
						<h4 id="SpotlightTitle" style="color: rgb(255, 0, 255);">&nbsp;</h4>
						<p><img alt="CONCRETE" src="/assets/city_files/pdfs/new%20council%202015%20web.jpg" style="width: 210px; height: 221px;" /></p>
						<p>&nbsp;</p>
						<b>2015 Council Recongnized</b>
						<strong style="line-height: 1.6em;"><u>Monday, October 10&nbsp;</u></strong>
						<p style="text-align: left;"><strong>City Council Meeting</strong><br />
						When:5:00 PM</p>
						<p style="text-align: left;">Where: Mt. Carmel City Hall</p>
						<p style="text-align: left;">&nbsp;</p>
						<p style="text-align: left;">&nbsp;</p>
						<p style="text-align: left;"><strong><a href="http://wabashcountychamber.com/events.htm" target="_blank">Calendar of Events</a></strong></p>
					</div>
				</div>
				<div class="city_updates threecol" id="news">
					<h3>News</h3>
					<ul>
						<li><a href="https://www.courtmoney.com/makepayment/payment.php?testID=2411" target="_blank"><img alt="" src="/assets/city_files/pdfs/Water%20Payments.jpg" style="width: 300px; height: 90px;" /></a></li>
						<li>Online Bill Pay for customers of Mount Carmel&#39;s Water / Sewer service.
						<ul>
							<li>Click on picture above. This will log into a secure server for online payments.</li>
						</ul>
						</li>
						<li><a href="mailto:council@cityofmtcarmel.com?subject=Council%20%26%20Mayor">City Council Email </a> To contact Mayor or Commissioners click on link</li>
						<li><a class="twitter-follow-button" data-show-count="false" href="https://twitter.com/CityofMtCarmel">Follow @CityofMtCarmel</a></li>
						<li><strong><a href="/assets/city_files/pdfs/CCR.pdf" target="_blank">Customer Confidence Report</a> </strong>Please click on link to view the annual drinking water quality report</li>

						<li><a href="/assets/city_filesfiles/pdfs/Ord%201047%202016%20-%202017%20municipal%20budget.pdf" target="_blank">2016 - 2017  Municipal Budget</a> Please click on link to view the Municipal budget</li>
						<li><a href="/assets/city_files/pdfs/trashschedule.jpg" target="_blank">Trash Pickup schedule</a> Please click on link to view the trash pickup schedule</li>
						<li>The City of Mount Carmel has implemented emailing the combined water sewer trash bills to those that want to participate. You also have the option of having them faxed to you. We have already received email addresses from customers that would like to participate. So please let us know if you too would like to help us save money by participating in the program. <a href="mailto:mgidcumb@cityofmtcarmel.com?subject=GO%20GREEN&amp;body=i%20would%20like%20to%20participate%20in%20email%20billing.">GO GREEN!</a></li>
						<li>Other Programs Offered:</li>
						<li>Pay your Utility Bill by Direct Debit. No more filling out checks and mailing, or worrying about whether the payment gets here on time. You can now have your utility bill payment deducted automatically from your bank account each month. If you are interested complete the <a href="/city_files/pdfs/Automaticbankpay.pdf" target="_blank">authorization form</a> and bring to City Hall at 219 N Market Street.</li>
						<li><a href="http://mcaea.com/" target="_blank"><img alt="" src="http://cityofmtcarmel.com/index_25_2523381700.png" style="width: 200px; height: 128px;" /></a></li>
						<li>The City of Mount Carmel is proud to announce the establishment of <a href="http://mcaea.com/" target="_blank">Area Economic Alliance</a>.  We invite you to follow the link to learn how this organization will become a vital part of our community&#39;s success.</li>
					</ul>
				</div>
				<div id="events" class="city_updates threecol">
				<h3>Events</h3>
				<div id="Events">
					<p><a href="/assets/city_files/pdfs/2016%20meetings%20and%20holiday.pdf" target="_blank"><strong>2016 COUNCIL MEETINGS   HOLIDAYS</strong></a></p>

					<p><strong>Agenda<br />
					<br />
					CITY OF MOUNT CARMEL</strong><br />
					<br />
					<strong>COUNCIL MEETING</strong><br />
					<br />
					<br />
					<strong>Monday Oct 10, 2016</strong><br />
					&nbsp;</p>

					<ul>
						<li>Pledge of Allegiance</li>
						<li><span style="line-height: 1.6em;">Invocation</span></li>
						<li>Roll Call</li>
						<li>Visitors to address the Council  (<u>topics discussed by visitors is limited to 5 minutes)</u>
						</li>
						<li><span style="line-height: 1.6em;">Mayor&rsquo;s Report</span></li>
					</ul>

					<p>
					<u>COMMISSIONER&rsquo;S REPORT:</u><br />
					<br />
					&bull; Joseph Judge ......... Finance and Senior Citizens<br />
					<br />
					&bull; Eric Ikemire ..... Streets, Cemeteries and Parks<br />
					<br />
					&bull; Rod Rodriguez.....Fire, Health  Safety, City Hall, Garbage and Civil Defense<br />
					<br />
					&bull; Justin Dulgar ..Water and Sewer<br />
					<br />
					<u>STAFF REPORTS:</u><br />
					<br />
					&bull; Rudy Witsman </p>

					<p><span style="line-height: 1.6em;">&bull; Tom Price </span></p>

					<p>&bull; Mike Gidcumb</p>

					<p><u style="line-height: 1.6em;">BUSINESS:</u></p>

					<ul>
						<li>Presentation of Consent Agenda
						<ul>
							<li>Approve the minutes of regular Council meeting held 9/26/2016</li>
							<li>Approve payment of all bills bearing proper signature</li>
						</ul>
						</li>
						<li>Removal of items from the Consent Agenda</li>
						<li>Consideration of Consent Agenda</li>
						<li>Consideration of items removed from Consent Agenda</li>
						<li>Executive Session (if needed)</li>
						<li>Motion to Adjourn</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="clear"></div>
		</div> <!-- Close .row -->
		</section>
	</div><!--Main Wrap close-->




<?php
include('includes/footer.php');
?>