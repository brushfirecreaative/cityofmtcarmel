<? 
/**
 * Mailer.php
 *
 * The Mailer class is meant to simplify the task of sending
 * emails to users. Note: this email system will not work
 * if your server is not setup to send mail.
 *
 */
 
class Mailer
{
   /**
    * sendWelcome - Sends a welcome message to the newly
    * registered user, also supplying the username and
    * password.
    */
   function sendWelcome($user, $email, $pass){
      $from = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDR.">";
      $subject = "City of Mount Carmel - Welcome!";
      $body = $user.",\n\n"
             ."Welcome! You can now login to the City of Mount Carmel Website "
             ."with the following information:\n\n"
             ."Username: ".$user."\n"
             ."Password: ".$pass."\n\n"
             ."If you ever lose or forget your password, a new "
             ."password will be generated for you and sent to this "
             ."email address, if you would like to change your "
             ."email address you can do so by going to the "
             ."My Account page after signing in.\n\n"
             ."- City Of Mount Carmel";

      return mail($email,$subject,$body,$from);
   }
   
   function newUser($email, $user, $pass, $slug, $name){
      $headers = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDR.">";
	  $headers .= "MIME-Version: 1.0\r\n";
	  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
      $subject = "Lawrence County Chamber of Commerce - Welcome!";
      $body = "<p style='text-align:center;'><img src='http://lawrencecountychamberofcommerce.com/images/email_logo.png' /></p>"
      		 ."<h1>Welcome ".$name."!</h2>"
      		 ."<p>You've just registered at Lawrence County Chamber's Website with the following information:</p>"
             ."<p>Username: <b>".$user."</b></p>"
             ."<p>Password: <b>".$pass."</b></p>"
             ."<p>You can view your page <a href=".SITEURL."/member-details/member/".$slug.">Here</a></p>"
             ."<p>If you ever lose or forget your password, a new "
             ."password will be generated for you and sent to this "
             ."email address, if you would like to change your "
             ."email address you can do so by going to the "
             ."My Account page after signing in.</p>"
             ."- <i>Lawrence County Chamber</i>";

      return mail($email,$subject,$body,$headers);
   }

   
   /**
    * sendNewPass - Sends the newly generated password
    * to the user's email address that was specified at
    * sign-up.
    */
   function sendNewPass($user, $email, $pass){
      $from = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDR.">";
      $subject = "Lawrence County Chamber of Commerce - Your new password";
      $body = $user.",\n\n"
             ."We've generated a new password for you at your "
             ."request, you can use this new password with your "
             ."username to log in to.\n\n"
             ."Username: ".$user."\n"
             ."New Password: ".$pass."\n\n"
             ."It is recommended that you change your password "
             ."to something that is easier to remember, which "
             ."can be done by going to the My Account page "
             ."after signing in.\n\n"
             ."- Lawrence County Chamber of Commerce";
             
      return mail($email,$subject,$body,$from);
   }
   
   function sendContactForm($name, $from_email, $email, $message){
      $from = "From: ".$name." <".$from_email.">";
      $subject = "Mundy Real Estate Website Contact";
      $body = "Contact Name: ".$name ."\n\n"
      		  ."Contact Email: " .$from_email ."\n\n"
      		  ."Message:\n\n"
      		  .$message;
             
      return mail($email,$subject,$body,$from);
   }

};

/* Initialize mailer object */
$mailer = new Mailer;
 
?>
