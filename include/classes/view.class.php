<?

class IGNview
{

	
	function pageEdit($page){
		global $database;
		$c = $database->getPageContent($page);
		?>
		<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(1)').addClass('select');
			});
		</script>
		<form id="edit" method="post" action="process.php">
		<div class="widget">
        	<div class="header"><span><span class="ico gray window"></span><?=$c['title']?></span></div>
            	<div class="content">
            			<div class="section">
            				<label> Page Title <small>This will show up on the page</small></label>
            				<div> <input type="text" name="title" id="title" class="title full" value="<?=$c['title'];?>"  /><span id="slug-wrap"><p >Permalink: (<input  name="slug" id="slug" value="<? if($page != 'new'){echo $c['slug'];}?>" class="slug" />)</p></span></div>
            				<label>Active <small>If this box is checked the page will show up on the site.</small></label>
            			<div><input type="checkbox" name="status" <? if($c['status'] == 'Active'){echo("checked");} ?> value="Active" id="status" /></div><br />	

            			</div>
            			<div class="boxtitle"> <span class="ico gray paragraph_align_left "></span>Page Content</div>	
                        <div> <textarea name="pageContent" id="editor2"  class="editor"  cols="80" rows="10" ><?=$c['content'];?></textarea></div><br />
                        <script>
	                        CKEDITOR.replace( 'pageContent');
	                    </script>
						<div class="section">
                              <label>Side Menu <small>This is the side menu you would like to appear on this page</small></label>   
                              <div>
                              	  <select name="menu" class="chzn-select"> 
                              	  	<option value="<?=$c['menu']?>"><? if($c['menu'] == ''){echo "No Menu"; }else{ echo $c['menu']; }?></option>
                                    <? $database->getMenus(); ?>
                                </select>       
                        </div>
            				
            			</div>
            			
            		
              </div>
        </div>                
		<div class="widget">
        	<div class="content">
        		<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="submit"/> <a class="uibutton special"  >clear form</a> <a class="uibutton loading confirm" title="Checking" rel="0" >Check</a> </div>
                </div>
        	</div>
        </div>
        <input type="hidden" name="id" value="<?=$c['id']?>" />
        <? if(($page == 'new')){
	        echo '<input type="hidden" name="func" value="add" />';
        }else{
	        echo'<input type="hidden" name="func" value="edit" />';
        } ?>
		<input type="hidden" name="editPage" value="1" />
		</form>
<? 
	}
		function fpEdit(){
		global $database;
		?>
		<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(4)').addClass('select');
			});
		</script>
		<form id="" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray pencil"></span>Scroller</span></div>
			<div class="content">	
                	<div><textarea name="pageContent4" id="editor2"  class="editor"  cols="10" rows="10" ><?=$database->getFPContent('scroller')?></textarea></div><br />	
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="Save Changes"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="fpEdit" value="scroller" />
		</form>
		<form id="" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray pencil"></span>Spotlight</span></div>
			<div class="content">	
                	<div><textarea name="pageContent" id="editor2"  class="editor"  cols="80" rows="10" ><?=$database->getFPContent('spotlight')?></textarea></div><br />	
                	 <script>
	                        CKEDITOR.replace( 'pageContent');
	                    </script>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="Save Changes"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="fpEdit" value="spotlight" />
		</form>
		<form id="" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray pencil"></span>News</span></div>
			<div class="content">	
                	<div><textarea name="pageContent2" id="editor3"  class="editor"  cols="80" rows="10" ><?=$database->getFPContent('news')?></textarea></div><br />	
                	<script>
	                        CKEDITOR.replace( 'pageContent2');
	                    </script>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="Save Changes"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="fpEdit" value="news" />
		</form>
		<form id="" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray pencil"></span>Events</span></div>
			<div class="content">	
                	<div><textarea name="pageContent3" id="editor4"  class="editor"  cols="80" rows="10" ><?=$database->getFPContent('events')?></textarea></div><br />	
                	<script>
	                        CKEDITOR.replace( 'pageContent3');
	                    </script>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="Save Changes"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="fpEdit" value="events" />
		</form>
        		
		
	<? }
	function sliderImageEdit($id){
	global $database;
	$c = $database->getSliderImageContent($id);
	?>
	<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(5)').addClass('select');
			});
		</script>
	<form id="addMember" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray dimensions"></span>Slider Image</span></div>
			<div class="content">
				<img src="../<?=$c['slide_image']?>" width="100%" />
				<div class="section">
					<label>Slide Title<small>The title is for SEO and Universal Access </small></label>
					<div><input type="text" name="slide_title" maxlength="50" id="name" class=" medium" value="<?=$c['slide_title']?>" /><br /></div>
					<label>Slide Text<small>This will be visible on the image it's self</small></label>
					<div><input type="text" name="slide_text" maxlength="50" id="name" class=" medium" value="<?=$c['slide_text']?>" /><br /></div>
					<label>Slide Status</label>
					<div>
                              	  <select class="chzn-select" name="status"> 
                              	  	<option value="<?=$c['slide_status']?>"><? if($c['slide_status'] == ''){echo "No Status"; }else{ echo $c['slide_status']; }?></option>
                                    <option value="Active">Active</option>
                                    <option value="In Active">In Active</option>
                                </select>
				</div>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Saving" value="Save Changes"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="editSlide" value="<?=$id?>" />
		</form>
				
		</div>
	<? }
	function addSlide(){
	?>
	<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(5)').addClass('select');
			});
		</script>
		<form id="addMember" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray dimensions"></span>Slider Image</span></div>
			<div class="content">
				<img class="preview" src="images/image_placehold.jpg" style="width:100%;" width="970" height="436" />
				<div class="section">
					<label>Slide Image<small>Upload a new image to be used on the slide.</small></label>
					<div> 
						
                    	<input type="file" name="slide"  id="slide" /><p>Slide should be 970x346</p>
                        <input type="hidden" name="slide_image" class="upload_input" value="" id="slide_image" />
                    </div><br />
					<label>Slide Title<small>The title is for SEO and Universal Access </small></label>
					<div><input type="text" name="slide_title" maxlength="50" id="name" class=" medium" value="" /><br /></div>
					<label>Slide Text<small>This will be visible on the image it's self</small></label>
					<div><input type="text" name="slide_text" maxlength="50" id="name" class=" medium" value="" /><br /></div>
					<label>Slide Status</label>
					<div>
                              	  <select class="chzn-select" name="status"> 
                                    <option value="Active">Active</option>
                                    <option value="In Active">In Active</option>
                                </select>
				</div>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Adding" value="Add New Slide"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="addSlide" value="1" />
		</form>
				
		</div>

	<? }
	function editProfile($user){
		global $database;
		$c = $database->getProfile($user);
	?>
	<form id="editProfile" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray user"></span>Profile Settings</span></div>
			<div class="content">
				<div class="section">
					<label>Username</label>
					<div><input type="text" name="username" class="upload_input" value="<?=$c['username']?>" id="username" />
                    </div><br />
					<label>Password<small>change password </small></label>
					<div><input type="password" name="password" maxlength="50" id="password" class=" medium" value="" /><div class="password_check red"></div></div>
					<label>Re-type password<small>This will be visible on the image it's self</small></label>
					<div><input type="password" name="password_ck" maxlength="50" id="password_ck" class=" medium" value="" /><br /></div>
					<script>
						$("#password_ck").focusout(function(){
							var pass1 = $("input#password").val();
							var pass2 = $("input#password_ck").val();
							if(pass1 != pass2){
							 $(".password_check").html("<p style='color:red;'>Passwords do not match</p>");
							}else{	
							$(".password_check").html("<p style='color:green;'>Passwords Match</p>");}
						});

					</script>
					<label>Slide Image<small>Upload a new image to be used on the slide.</small></label>
					<div> 
						
                    	<input type="file" name="avatar"  id="avatar" /><p>Avatar should be 200x200</p>
                        <input type="hidden" name="image" class="upload_input" value="" id="slide_image" />
                    </div>
                    <div><img class="preview" src="images/image_placehold.jpg" style="width:200px;" width="200" height="200" /></div>
				<div class="section last">
                	<div><input type="submit" class="uibutton loading" title="Adding" value="Add New Slide"/></div>
                </div>
        	</div>
        </div>
		<input type="hidden" name="editProfile" value="1" />
		</form>
				
		</div>

<? }
	//Edit Agents
	function personEdit(){
		global $session, $database;
		$person = $_GET['person'];
		$c = $database->getPerson($person);
		?>
		<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(3)').addClass('select');
			});
		</script>
		<form id="editPerson" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray user"></span><? if(isset($c['name'])){ ?> Edit Person: <?=$c['name']?> <? }else{ echo "Add New Staff Member";} ?> </span></div>
			<div class="content">
				<div class="section">
					<label>Staff Member Name<small>Full name of the staff member</small></label>
					<div><input type="text" name="name" class=" medium" value="<?=$c['name']?>" id="name" /></div>
					<label>Active<small>If the staff member is active check</small></label>
					<div><input type="checkbox" name="status" <? if($c['status'] == 'Active'){echo("checked");} ?> value="Active" id="status" /></div><br />
					<label>Title<small>The title or position this person holds</small></label>
					<div><input type="text" name="title"  id="title" class=" medium" value="<?=$c['title']?>" /></div>
					<label>Department<small>The Department this person works in</small></label>
					<div> <input  type="text" name="group" id="group" value="<?=$c['person_group']?>" /></div>
					<label>Mobile<small></small></label>
					<div><input type="text" name="cell"  id="phone" class=" medium" value="<?=$c['phone']?>" /></div>
					<label>Email<small></small></label>
					<div><input type="text" name="email"  id="email" class=" medium" value="<?=$c['email']?>" /></div>
					<label>Bio</label>
					<div><textarea name="bio" cols="5" rows="10"></textarea></div>
					<script>
						CKEDITOR.replace( 'bio');
					</script>
					<label>Photo<small>Image should be 250x344px</small></label>
					<div> 
                    	<input type="file" name="person_image"  id="person_image" /><p></p>
                        <input type="hidden" name="image" class="upload_input" value="<?=$c['img']?>" id="image" /><p>Expectable file formats are: .jpg, .png, .pdf, .tif  </p>
                    </div>
                    <div><img class="thumbnail" src="<?=$c['img']?>" style="" width="250" height="344" /></div>
				<div class="section last">
                	<input type="submit" class="uibutton loading" title="Updating" value="<? if(isset($agent)){echo 'Update Person';}else{echo 'Add Person';} ?>"/>
                </div>
        	</div>
        </div>
        <input type="hidden" name="id" id="id" value="<?=$person?>" />
       
		<input type="hidden" name="updatePerson" value="<? if(isset($person)){echo 'edit';}else{echo 'add';} ?>" />
		</form>
				
		</div>

		
	<? }
	function editPreferences(){ ?>
		<script>
			$(document).ready(function(){
				$('#main_menu li').removeClass('select');
				$('#main_menu li:eq(5)').addClass('select');
			});
		</script>
		<form id="addAmeneties" action="process.php" method="POST">
		<div class="widget">
			<div class="header"><span><span class="ico gray user"></span>Add New Amenities</span></div>
			<div class="content">
				<div class="section">
					<label>Amenity<small>the name of the new amenity</small></label>
					<div><input type="text" name="option_value" class=" medium" value="" id="option_value" /><input type="submit" class="uibutton loading" title="Adding" value="Add New Amenity"/></div><br />
				</div>

			</div>
        </div>
		<input type="hidden" name="addOption" value="1" />
		<input type="hidden" name="option_name" value="amenity" />
		</form>
		
	<? }
}
$IGNview = new IGNview; ?>