<?
/**
 * Database.php
 * 
 * The Database class is meant to simplify the task of accessing
 * information from the website's database.
 *
 * Written by: Brushfire Design & Eric Campagna
 *
 */
include("constants.php");
      
class MySQLDB
{
   var $connection;         //The MySQL database connection
   var $num_active_users;   //Number of active users viewing site
   var $num_active_guests;  //Number of active guests viewing site
   var $num_members;        //Number of signed-up users
   /* Note: call getNumMembers() to access $num_members! */

   /* Class constructor */
   function MySQLDB(){
      /* Make connection to database */
      $this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS) or die(mysql_error());
      mysql_select_db(DB_NAME, $this->connection) or die(mysql_error());
      
      /**
       * Only query database to find out number of members
       * when getNumMembers() is called for the first time,
       * until then, default value set.
       */
      $this->num_members = -1;
      
      if(TRACK_VISITORS){
         /* Calculate number of users at site */
         $this->calcNumActiveUsers();
      
         /* Calculate number of guests at site */
         $this->calcNumActiveGuests();
      }
   }
   /**
   	* Settings
   	*
   	*
   	*
   	*/
   	function get_option($option){
   		$q = "SELECT option_value FROM ign_options WHERE option_name='$option'";
   		$r = mysql_query($q);
   		$row = mysql_fetch_row($r);
   		return $row[0];
   	}
   	
   	function getGlobals(){
   		$q = "SELECT * FROM ign_options";
   		$r = mysql_query($q, $this->connection);
   		/* Error occurred, return given name by default */
      	if(!$result || (mysql_num_rows($result) < 1)){
         return NULL;
      	}
      	/* Return result array */
      	$dbarray = mysql_fetch_array($result);
      	return $dbarray;
   	}
   /**
    * confirmUserPass - Checks whether or not the given
    * username is in the database, if so it checks if the
    * given password is the same password in the database
    * for that user. If the user doesn't exist or if the
    * passwords don't match up, it returns an error code
    * (1 or 2). On success it returns 0.
    */
   function confirmUserPass($username, $password){
      /* Add slashes if necessary (for query) */
      if(!get_magic_quotes_gpc()) {
	      $username = addslashes($username);
      }

      /* Verify that user is in database */
      $q = "SELECT password FROM ".TBL_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      if(!$result || (mysql_numrows($result) < 1)){
         return 1; //Indicates username failure
      }

      /* Retrieve password from result, strip slashes */
      $dbarray = mysql_fetch_array($result);
      $dbarray['password'] = stripslashes($dbarray['password']);
      $password = stripslashes($password);

      /* Validate that password is correct */
      if($password == $dbarray['password']){
         return 0; //Success! Username and password confirmed
      }
      else{
         return 2; //Indicates password failure
      }
   }
   
   /**
    * confirmUserID - Checks whether or not the given
    * username is in the database, if so it checks if the
    * given userid is the same userid in the database
    * for that user. If the user doesn't exist or if the
    * userids don't match up, it returns an error code
    * (1 or 2). On success it returns 0.
    */
   function confirmUserID($username, $userid){
      /* Add slashes if necessary (for query) */
      if(!get_magic_quotes_gpc()) {
	      $username = addslashes($username);
      }

      /* Verify that user is in database */
      $q = "SELECT userid FROM ".TBL_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      if(!$result || (mysql_numrows($result) < 1)){
         return 1; //Indicates username failure
      }

      /* Retrieve userid from result, strip slashes */
      $dbarray = mysql_fetch_array($result);
      $dbarray['userid'] = stripslashes($dbarray['userid']);
      $userid = stripslashes($userid);

      /* Validate that userid is correct */
      if($userid == $dbarray['userid']){
         return 0; //Success! Username and userid confirmed
      }
      else{
         return 2; //Indicates userid invalid
      }
   }
   
   /**
    * usernameTaken - Returns true if the username has
    * been taken by another user, false otherwise.
    */
   function usernameTaken($username){
      if(!get_magic_quotes_gpc()){
         $username = addslashes($username);
      }
      $q = "SELECT username FROM ".TBL_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      return (mysql_numrows($result) > 0);
   }
   
   /**
    * usernameBanned - Returns true if the username has
    * been banned by the administrator.
    */
   function usernameBanned($username){
      if(!get_magic_quotes_gpc()){
         $username = addslashes($username);
      }
      $q = "SELECT username FROM ".TBL_BANNED_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      return (mysql_numrows($result) > 0);
   }
   
   /**
    * addNewUser - Inserts the given (username, password, email)
    * info into the database. Appropriate user level is set.
    * Returns true on success, false otherwise.
    */
   function addNewUser($username, $password, $email){
      $time = time();
      /* If admin sign up, give admin user level */
      if(strcasecmp($username, ADMIN_NAME) == 0){
         $ulevel = ADMIN_LEVEL;
      }else{
         $ulevel = USER_LEVEL;
      }
      $q = "INSERT INTO ".TBL_USERS."(username, password, userid, userlevel, email, timestamp) VALUES ('$username', '$password', '0', $ulevel, '$email',$time)";
      return mysql_query($q, $this->connection) or die(mysql_error());
   }
   
   /**
    * updateUserField - Updates a field, specified by the field
    * parameter, in the user's row of the database.
    */
   function updateUserField($username, $field, $value){
      $q = "UPDATE ".TBL_USERS." SET ".$field." = '$value' WHERE username = '$username'";
      return mysql_query($q, $this->connection);
   }
   
/*** Preferences ***/
function set_option($option_name, $option_value){
   		$q = "INSERT into options (option_name, option_value, autoload) VALUES ('$option_name', '$option_value', 'Yes')";
   		$r = mysql_query($q, $this->connection) or die( mysql_error());
   		return true;
   	}
   	function get_options($option){
   		$q = "SELECT * FROM options WHERE option_name='$option' ORDER BY option_value ASC";
   		$r = mysql_query($q);
   		return $r;
   		
   			
   	}
/**************************************************************************************************** Update Section ********************************************************************/  
/* Update Obituary */
	function updateObit($slug, $picture, $name, $dates, $d_date, $military, $obit, $id){
		$q ="UPDATE obituaries SET slug='$slug', picture='$picture', name='$name', dates='$dates', d_date='$d_date', military='$military', obit='$obit' WHERE id='$id'";
		$r = mysql_query($q, $this->connection) or die(mysql_error());
	} 
/* Update Slide Info */
	function updateSlideInfo($title, $text, $status, $id){
		$q ="UPDATE fp_slider SET slide_title='$title', slide_text='$text', slide_status='$status' WHERE id='$id'";
		$r = mysql_query($q, $this->connection) or die(mysql_error());
	}
/* Update Front Page Content */
	function updateFPContent($name, $content){
		$q ="UPDATE fp_news SET content='$content' WHERE section='$name'";
		$r = mysql_query($q, $this->connection) or die(mysql_error());
	}
/* Update Settings */
	function setOption($option_name, $option_value){
		$q ="UPDATE ign_options SET option_value='$option_value' WHERE option_name='$option_name'";
		$r = mysql_query($q, $this->connection) or die(mysql_error());
		}
/* Update Pages */
   function updatePage($id, $slug, $title, $content, $author, $menu){
      $q = "UPDATE pages SET slug='$slug', content='$content', author='$author', title='$title', menu='$menu' WHERE id = '$id'";
      $r = mysql_query($q, $this->connection) or die(mysql_error());
   } 
/**************************************************************************************************** Add New Section ********************************************************************/  
/* Add New Obituary  */
	function insertObit($slug, $picture, $name, $dates, $d_date, $military, $obit){
		$q = "INSERT into obituaries (slug, picture, name, dates, d_date, military, obit) VALUES ('$slug', '$picture', '$name', '$dates', '$d_date', '$military', '$obit')";
		$r = mysql_query($q, $this->connection) or die( mysql_error( "Could not add new Obituary"));
	}
/* Add New Slide for ign_slider */
	function addNewSlide($img, $title, $text, $status){
		$q = "INSERT into fp_slider (slide_image, slide_title, slide_text, slide_status) VALUES ('$img', '$title', '$text', '$status')";
		$r = mysql_query($q, $this->connection) or die( mysql_error( "Could not add new Slide"));
	}
/* Add New Ignite User */    
    function addUser($username, $pass, $userlevel, $name){
    	$password = md5($pass);
    	$q = "INSERT into users (username, password, userlevel, full_name) VALUES ('$username', '$password', '$userlevel', '$name')";
    	$r = mysql_query($q, $this->connection) or die( mysql_error( "Could not add new User"));
    }
    /* Add New Page*/    
    function addPage($slug, $title, $content, $author, $menu, $status, $date){
    	$q = "INSERT into pages (slug, title, content, author, menu, status, date_posted) VALUES ('$slug', '$title', '$content', '$author', '$menu', '$status', '$date')";
    	$r = mysql_query($q, $this->connection) or die(mysql_error());
    }
/**************************************************************************************************** Get Information Section ********************************************************************/
/* Get Menu for Select Page */
	function getPageMenu($page){
		$q = "SELECT menu FROM pages WHERE slug='$page'";
		$result = mysql_query($q, $this->connection) or die(mysql_error());
		if(!$result || (mysql_num_rows($result) < 1)){
		
			$q2 = "SELECT * FROM links WHERE menu='$page'";
			$result2 = mysql_query($q2, $this->connection) or die(mysql_error());
			return $result2;
        }else{
        $menu = mysql_fetch_row($result);
        
        $q3 = "SELECT * FROM links WHERE menu='$menu[0]'";
        $result3 = mysql_query($q3, $this->connection) or die(mysql_error());
        return $result3;
        }
	}
/* Get Page Contents */
	function getPageContents($page){
		$q = "SELECT * FROM pages WHERE slug='$page'";
		$result = mysql_query($q, $this->connection);
        if(!$result || (mysql_num_rows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      return $dbarray;
	}
/* Get Profile Info */
	function getProfile($user){
	$q = "SELECT username, userlevel, full_name, image FROM users WHERE username='$user'";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_num_rows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      return $dbarray;
	}
   function getSliderImageContent($id){
   	$q = "SELECT * FROM fp_slider WHERE id='$id'";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_num_rows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      return $dbarray;
   }
   function getSliderImages(){
   	 $q = "SELECT * FROM fp_slider ORDER BY list_order ASC";
      $r = mysql_query($q, $this->connection);
      $table = 'fp_slider';
      /* Error occurred, return given name by default */
         if(!$r || (mysql_num_rows($r) < 1)){
   			echo '<h1>No Pages Found</h1>';
   		}
   		if ( $r !== false && mysql_num_rows($r) > 0 ) {
      		while ( $a = mysql_fetch_assoc($r) ) {
      		 $id = stripcslashes($a['id']);
       		 $title = stripslashes($a['slide_title']);
       		 $img = stripslashes($a['slide_image']);
       		 $text = stripslashes($a['slide_text']);
       		 $status = stripcslashes($a['slide_status']);
       		 $list_order = stripcslashes($a['list_order']);
   			echo 	'<tr>'
   					.'<td  width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check' .$id  .'"/></td>'
                                    .'<td  align="left"><img src="../'.$img.'" width="350" /></td>'
                                    .'<td >' .$title.'</td>'
                                    .'<td >' .$status .'</td>'
                                    .'<td >'
                                     .' <span class="tip" >'
                                        .'  <a  title="Edit" href="?p=edit&slider_image=' .$id .'" >'
                                            .'  <img src="images/icon/icon_edit.png" >'
                                         .' </a>'
                                      .'</span> '
                                     .' <span class="tip" >'
                                         .' <a id="'.$id.'" class="Delete"  name="'.$title.'" title="Delete" href="process.php?action=delete&t='.$table.'&c=id&i='.$id.'"   >'
                                         .'     <img src="images/icon/icon_delete.png" >'
                                         .' </a>'
                                     .' </span> '
                                      	.' </td>'
                                      .'</tr>';
   			
   		}
   }

      }
   function getFPContent($name){
   	$q = "SELECT content FROM fp_news WHERE section='$name'";
   		$r = mysql_query($q);
   		$row = mysql_fetch_row($r);
   		return $row[0];
   }
   function getUserInfo($username){
      $q = "SELECT * FROM ".TBL_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_num_rows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      return $dbarray;
   }
   function getRecentObits(){
   		$today = date("Y/m/d");
	   $q = "SELECT * FROM obituaries ORDER BY d_date LIMIT 4";
      $result = mysql_query($q, $this->connection);
      return $result;
   }
   function getObit($obit_name){
	   $q = "SELECT * FROM obituaries WHERE slug = '$obit_name'";
      $result = mysql_query($q, $this->connection);
      return $result;
   }
   function IGNgetObits(){
	   $q = "SELECT * FROM obituaries ORDER BY d_date";
	   $r = mysql_query($q, $this->connection);
	   $table = "obituaries";
	   /* Error occurred*/
   		if(!$r || (mysql_num_rows($r) < 1)){
   			echo '<h1>No Pages Found</h1>';
   		}
   		if ( $r !== false && mysql_num_rows($r) > 0 ) {
      		while ( $a = mysql_fetch_assoc($r) ) {
       		 $name = stripslashes($a['name']);
       		 $d_date = stripslashes($a['d_date']);
       		 $picture = stripslashes($a['picture']);
       		 $id = stripcslashes($a['id']);
       		 $slug = stripcslashes($a['slug']);
   			echo 	'<tr>'
   					.'<td  width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check' .$id  .'"/></td>'
                                    .'<td  align="left"><a  title="Edit" href="?p=edit&obituary=' .$slug .'" ><img src="http://scfh.brushfire-design.com/'.$picture .' "/></a></td>'
                                    .'<td >' .$name.'</td>'
                                    .'<td >' .$d_date .'</td>'
                                    .'<td >'
                                     .' <span class="tip" >'
                                        .'  <a  title="Edit" href="?p=edit&page=' .$slug .'" >'
                                            .'  <img src="images/icon/icon_edit.png" >'
                                         .' </a>'
                                      .'</span> '
                                     .' <span class="tip" >'
                                         .' <a id="1" class="Delete"  name="Band ring" title="Delete" href="process.php?action=delete&t='.$table.'&c=id&i='.$id.'"    >'
                                         .'     <img src="images/icon/icon_delete.png" >'
                                         .' </a>'
                                     .' </span> '
                                      	.' </td>'
                                      .'</tr>';
   			
   		}
   }

   }
// FUNCTIONS for dealing with PEOPLE /////////////////////////////////////////////////

/* Get Single Person Info */
	function getPerson($person){
		$q = "SELECT * FROM people WHERE id='$person' OR slug='$person'";
		$result = mysql_query($q, $this->connection) or die(mysql_error());
        if(!$result || (mysql_num_rows($result) < 1)){
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      return $dbarray;
	}
/* Get People */
	function getPeople(){
	$q = "SELECT * FROM people WHERE status='Active' ORDER BY name ASC";
    $r = mysql_query($q, $this->connection);
        return $r;
	}
	/* Get All People */
	function IGNgetPeople(){
	$q = "SELECT * FROM people";
    $r = mysql_query($q, $this->connection);
        return $r;
	}
	/* Update Person */
 function updatePerson($name, $slug, $status, $title, $phone, $email, $bio, $image, $id, $group){
	 $q = "UPDATE people SET name='$name', title='$title', img='$image', phone='$phone', email='$email', bio='$bio', slug='$slug', person_group='$group', status='$status' WHERE id='$id'";
	 $r = mysql_query($q, $this->connection) or die(mysql_error());
 }
 /* Add Person */
 function addPerson($name, $slug, $status, $title, $phone, $email, $bio, $image, $group){
		$q = "INSERT INTO people (name, slug, status, title, phone, email, bio, img, person_group) Values ('$name', '$slug', '$status', '$title', '$phone', '$email', '$bio', '$image', '$group')";
		$r = mysql_query($q, $this->connection) or die( mysql_error());
	}
//// IGN Quereies ////////////////////////////////////////////////////////////////////
   function getpages(){
   		$q = "SELECT * FROM pages";
   		$r = mysql_query($q, $this->connection);
   		/* Error occurred*/
   		if(!$r || (mysql_num_rows($r) < 1)){
   			echo '<h1>No Pages Found</h1>';
   		}
   		if ( $r !== false && mysql_num_rows($r) > 0 ) {
      		while ( $a = mysql_fetch_assoc($r) ) {
       		 $title = stripslashes($a['title']);
       		 $author = stripslashes($a['author']);
       		 $date = stripslashes($a['posted']);
       		 $id = stripcslashes($a['id']);
       		 $slug = stripcslashes($a['slug']);
   			echo 	'<tr>'
   					.'<td  width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check' .$id  .'"/></td>'
                                    .'<td  align="left"><a  title="Edit" href="?p=edit&page=' .$slug .'" >'.$title .'</a></td>'
                                    .'<td >' .$author.'</td>'
                                    .'<td >' .$date .'</td>'
                                    .'<td >'
                                     .' <span class="tip" >'
                                        .'  <a  title="Edit" href="?p=edit&page=' .$slug .'" >'
                                            .'  <img src="images/icon/icon_edit.png" >'
                                         .' </a>'
                                      .'</span> '
                                     .' <span class="tip" >'
                                         .' <a id="1" class="Delete"  name="Band ring" title="Delete"  >'
                                         .'     <img src="images/icon/icon_delete.png" >'
                                         .' </a>'
                                     .' </span> '
                                      	.' </td>'
                                      .'</tr>';
   			
   		}
   }
   }
    function getCats($group){
   		$q = "SELECT * FROM category";
   		$r = mysql_query($q, $this->connection);
   		
   		if ( $r !== false && mysql_num_rows($r) > 0 ) {
      		while ( $a = mysql_fetch_assoc($r) ) {
       		 $cat = stripslashes($a['cat_name']);
       		 $slug = stripslashes($a['slug']);
        
        echo "<option value='".$slug."'>".$cat."</option>";
			}
		}else{
			echo '<option value="">None found</option>';
		}
   }
  
   /**
    * getNumMembers - Returns the number of signed-up users
    * of the website, banned members not included. The first
    * time the function is called on page load, the database
    * is queried, on subsequent calls, the stored result
    * is returned. This is to improve efficiency, effectively
    * not querying the database when no call is made.
    */
   function getNumMembers(){
      if($this->num_members < 0){
         $q = "SELECT * FROM ".TBL_USERS;
         $result = mysql_query($q, $this->connection);
         $this->num_members = mysql_numrows($result);
      }
      return $this->num_members;
   }
   
   /**
    * calcNumActiveUsers - Finds out how many active users
    * are viewing site and sets class variable accordingly.
    */
   function calcNumActiveUsers(){
      /* Calculate number of users at site */
      $q = "SELECT * FROM ".TBL_ACTIVE_USERS;
      $result = mysql_query($q, $this->connection);
      $this->num_active_users = mysql_numrows($result);
   }
   
   /**
    * calcNumActiveGuests - Finds out how many active guests
    * are viewing site and sets class variable accordingly.
    */
   function calcNumActiveGuests(){
      /* Calculate number of guests at site */
      $q = "SELECT * FROM ".TBL_ACTIVE_GUESTS;
      $result = mysql_query($q, $this->connection);
      $this->num_active_guests = mysql_numrows($result);
   }
   
   /**
    * addActiveUser - Updates username's last active timestamp
    * in the database, and also adds him to the table of
    * active users, or updates timestamp if already there.
    */
   function addActiveUser($username, $time){
      $q = "UPDATE ".TBL_USERS." SET timestamp = '$time' WHERE username = '$username'";
      mysql_query($q, $this->connection);
      
      if(!TRACK_VISITORS) return;
      $q = "REPLACE INTO ".TBL_ACTIVE_USERS." VALUES ('$username', '$time')";
      mysql_query($q, $this->connection);
      $this->calcNumActiveUsers();
   }
   
   /* addActiveGuest - Adds guest to active guests table */
   function addActiveGuest($ip, $time){
      if(!TRACK_VISITORS) return;
      $q = "REPLACE INTO ".TBL_ACTIVE_GUESTS." VALUES ('$ip', '$time')";
      mysql_query($q, $this->connection);
      $this->calcNumActiveGuests();
   }
   
   /* These functions are self explanatory, no need for comments */
   
   /* removeActiveUser */
   function removeActiveUser($username){
      if(!TRACK_VISITORS) return;
      $q = "DELETE FROM ".TBL_ACTIVE_USERS." WHERE username = '$username'";
      mysql_query($q, $this->connection);
      $this->calcNumActiveUsers();
   }
   
   /* removeActiveGuest */
   function removeActiveGuest($ip){
      if(!TRACK_VISITORS) return;
      $q = "DELETE FROM ".TBL_ACTIVE_GUESTS." WHERE ip = '$ip'";
      mysql_query($q, $this->connection);
      $this->calcNumActiveGuests();
   }
   
   /* removeInactiveUsers */
   function removeInactiveUsers(){
      if(!TRACK_VISITORS) return;
      $timeout = time()-USER_TIMEOUT*60;
      $q = "DELETE FROM ".TBL_ACTIVE_USERS." WHERE timestamp < $timeout";
      mysql_query($q, $this->connection);
      $this->calcNumActiveUsers();
   }

   /* removeInactiveGuests */
   function removeInactiveGuests(){
      if(!TRACK_VISITORS) return;
      $timeout = time()-GUEST_TIMEOUT*60;
      $q = "DELETE FROM ".TBL_ACTIVE_GUESTS." WHERE timestamp < $timeout";
      mysql_query($q, $this->connection);
      $this->calcNumActiveGuests();
   }
   
   /**
    * query - Performs the given query on the database and
    * returns the result, which may be false, true or a
    * resource identifier.
    */
   function query($query){
      return mysql_query($query, $this->connection);
   }
   /*
   -----------------------------------------------------------------------------
   Functions for Adding, Editing or Deleting Pages
   -----------------------------------------------------------------------------
   */
   function getPageContent($slug){
      $q = "SELECT * from pages WHERE slug = '$slug'";
      $result = mysql_query($q, $this->connection);
   		/* Error occurred*/
   		if(!$result || (mysql_num_rows($result) < 1)){
   			return NULL;
   		}
   		$dbarray = mysql_fetch_array($result);
   		return $dbarray;
      
   }
   function getMenus(){
   		$q = "SELECT * FROM menus where type = 'sidebar'";
   		$r = mysql_query($q, $this->connection);
   		
   		if ( $r !== false && mysql_num_rows($r) > 0 ) {
      		while ( $a = mysql_fetch_assoc($r) ) {
       		 $title = stripslashes($a['title']);
       		 $slug = stripslashes($a['slug']);
        
        echo "<option value='".$slug."'>".$title."</option>";
			}
		}
   }
   function deleteEntry($table, $col, $id, $member){
		$q = "DELETE FROM $table WHERE $col=$id";	
		$r = mysql_query($q, $this->connection) or die( mysql_error());
		
	}
};

/* Create database connection */
$database = new MySQLDB;

?>
