<?php 
/* Settings File 
 *
 *Contains all the settings that are able
 *to be changed from the admin screens
 *
 *Copyright: Ignite & Brushfire Design
 *Written by: Eric Campagna
 *
 */
 
 

 /*
  * Site Name
  */
  function site_name(){
  	global $database;
  	
  	$title = ' ~ ';	
  	$title .= $database->get_option('site_name');
  	
  	return $title;
  }
 define("SITENAME", site_name());
 
 /*
  * Site URL 
  */
 define("SITEURL", $database->get_option('site_url'));
 
 /*
  * Administrator URL 
  */
 define("ADMINRUL", $database->get_option('admin_url'));
 
 /**
 * Email Constants - these specify what goes in
 * the from field in the emails that the script
 * sends to users, and whether to send a
 * welcome email to newly registered users.
 */
define("EMAIL_FROM_NAME", $database->get_option('email_from_name'));
define("EMAIL_FROM_ADDR", $database->get_option('email_from_address'));
define("EMAIL_WELCOME", false);

define("SLIDER_DIR", SITEURL."/images/scoller_images/");
