<div class="main">
<a class="back" href="index.html">Back</a>
	<div class="content">
	<? if(isset($_SESSION['useredit'])){
   		unset($_SESSION['useredit']);
   		echo "<h1 class='success'>User Account Edit Success!</h1>";
   		
}

/* Requested Username error checking */
$req_user = trim($_GET['user']);
if(!$req_user || strlen($req_user) == 0 ||
   !eregi("^([0-9a-z])+$", $req_user) ||
   !$database->usernameTaken($req_user)){
   die("Username not registered");
}

/* Logged in user viewing own account */
if(strcmp($session->username,$req_user) == 0){
   echo "<h1>My Account</h1>";
}
/* Visitor not viewing own account */
else{
   echo "<h1>User Info</h1>";
}
if(strcmp($session->username,$req_user) == 0){
   echo "<a class='edit_dealer' href=\"useredit.php\">Edit Account Information</a><br>";
}
/* Display requested user information */
$req_user_info = $database->getUserInfo($req_user);

echo "<h2>".$session->dealername. "</h2><br />";
echo "<div class='form_wrap'>";
echo "<b>Username: ".$req_user_info['username']."</b><br>";
echo "<b>Email:</b> ".$req_user_info['email']."<br>";
echo "<b>Phone:</b> ".$session->dealerinfo['phone']."<br />";
echo "<b>Fax:</b> ".$session->dealerinfo['fax']."<br />";
echo "</div>";
echo "<div class='form_wrap'>";
echo "<h3>Billing Address:</h3>";
echo $session->userbilling['address1']."<br />";
echo $session->userbilling['address2'];
echo $session->userbilling['city']. ", ";
echo $session->userbilling['state']. " ";
echo $session->userbilling['zip'];
echo "</div>";
echo "<div class='form_wrap'>";
echo "<h3>Shipping Address:</h3>";
echo $session->usershipping['address1']."<br />";
echo $session->usershipping['address2'];
echo $session->usershipping['city']. ", ";
echo $session->usershipping['state']. "&nbsp;";
echo $session->usershipping['zip'];
echo "</div>";
/**
 * Note: when you add your own fields to the users table
 * to hold more information, like homepage, location, etc.
 * they can be easily accessed by the user info array.
 *
 * $session->user_info['location']; (for logged in users)
 *
 * ..and for this page,
 *
 * $req_user_info['location']; (for any user)
 */

/* If logged in user viewing own account, give link to edit */


/* Link back to main */


?>
	</div>
</div>
</body>
</html>
