<div id="orderForm">
            <h2>Order Form for Minivans</h2>
            <div id="wrapper">
	<div id="steps">
	<form class="transForm" id="formElem" name="formElem" action="process.php" method="post">
                        <fieldset class="step">
                            <legend>Contact Information</legend>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_name qm"></a>
                            	<em class="name tooltip">
								For our records, please tell us the person placing the order. <a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="name"><em class="required">*</em> Ordered By</label> 
                                <input id="name" name="name" type="text"  class="validate[required]" />
                                
                           
                            	
							</p>	
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_po qm"></a>
                            	<em class="po tooltip">
								If you have a specific Purchase Order Number please enter it now.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="po">P.O.</label>
                                <input id="po" name="po" type="text"  />
                            </p>
                        </fieldset>
                        <fieldset class="step">
                            <legend>Vehicle Information</legend>
                             <p>
                             	<a href="javascript:void(0)" class="show_hide_vyear qm"></a>
                            	<em class="vyear tooltip">
								The Year of the vehicle.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_year"><em class="required">*</em> Vehicle Year</label>
                                <input id="v_year" name="v_year" type="text"  class="validate[required] year" />
                            </p>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_vmake qm"></a>
                            	<em class="vmake tooltip">
								The Make of the vehicle.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_make"><em class="required">*</em> Make</label>
                                <input id="v_make" name="v_make" type="text"  class="validate[required]" />
                            </p>
                           <p>
                           		<a href="javascript:void(0)" class="show_hide_vmodel qm"></a>
                            	<em class="vmodel tooltip">
								The Model of the vehicle<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_model"><em class="required">*</em> Model</label>
                               <input id="v_model" name="v_model" type="text"  class="validate[required]" />
                            </p>
                           
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_driver qm"></a>
                            	<em class="driver tooltip">
								What kind of OEM Driver Seat is currently installed in the vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="driver_sseat"><em class="required">*</em> Type of Driver's Seat</label>
                                <select id="drivers_seat" name="drivers_seat" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="power">OEM Factory Power Seat</option>
                                    <option value="manual">OEM Factory Manual Seat</option>
                                </select>
                            </p>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_pass qm"></a>
                            	<em class="pass tooltip">
								What kind of OEM Passenger Seat is currently installed in the vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="pass_seat"><em class="required">*</em> Type of Passenger's Seat</label>
                                <select id="pass_seat" name="pass_seat" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="power">OEM Factory Power Seat</option>
                                    <option value="manual">OEM Factory Manual Seat</option>
                                </select>
                            </p>
                             <p>
                             	<a href="javascript:void(0)" class="show_hide_ramp qm"></a>
                            	<em class="ramp tooltip">
								What type of ramp is currently installed in the Vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="ramp"><em class="required">*</em> Type of Ramp</label>
                                <select id="ramp" name="ramp" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="floor">In-floor Ramp</option>
                                    <option value="folding">Folding Ramp</option>
                                    <option value="none">None</option>
                                </select>
                            </p>
                            <p>
                                <label for="conversion"><em class="required">*</em> Conversion By</label>
                                <input id="conversion" name="conversion" type="text"  class="validate[required]" />
                            </p>
							<p>
                                <label for="conversion_type"><em class="required">*</em> Conversion Type</label>
                                <input id="conversion_type" name="conversion_type" type="text"  class="validate[required]" />
                            </p>
                             <p>
                                <label for="conversion_year"><em class="required">*</em> Year of Conversion</label>
                                <input id="conversion_year" name="conversion_year" type="text"  class="validate[required] year" />
                            </p>
                             <p>
                                <label for="color"><em class="required">*</em> Interior Color</label>
                                <select id="color" name="color" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p>
                        </fieldset>
                        <fieldset class="step">
                            <legend>Leadership Seat Base Options</legend>
                            
							<p>
                            	<label for="l51"><b>L51 Leadership Series:</b> 6 Way for Lowered Floor Vans</label>
                            	Driver<input type="checkbox" id="l51" name="l51" value="l51-driver"/>
                            	Passenger<input type="checkbox" id="l51" name="l51" value="l51-passenger"/> 
                            	<em class="cover_option">                     	       	
                            		<label  for="cover"><small><em class="required">*</em> Decorative Cover for L51</small></label>
                            		<select  id="cover" name="cover" class="validate[required]">
                                		<option value="" selected="selected">Select...</option>
                                    	<option value="yes">Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    	<option value="not">No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                	</select>
                                </em>
                            </p> 
							<p>
                            	<label for="l53"><b>L53 Leadership Series:</b> 4 Way for Lowered Floor Vans</label>
                            	Driver<input type="checkbox" id="l53" name="l53" value="l53-driver"/>
                            	Passenger<input type="checkbox" id="l53" name="l53" value="l53-passenger"/>
                            </p>
							<p>
                            	<label for="l75"><b>L75 Leadership Series:</b> 4 Way for Non-Lowered Floor &amp; Rear Entry Vans </label>
                            	Driver<input type="checkbox" id="l75" name="l75" value="l75-driver"/>
                            	Passenger<input type="checkbox" id="l75" name="l75" value="l75-passenger"/>
                            </p>
							<p>
                            	<label for="powder">Powder Coat</label>
                            	<select id="powder" name="powder"/>
                            	<option value="none" selected="selected">Select...</option>
								<option value="gray" >Gray (standard)</option>
                                <option value="tan">Tan (additional fee)</option>
								</select>
                            </p>
						
							
                         </fieldset>
						<fieldset class="step">
						<fieldset class="step">
                            <legend>Options &amp; Accessories</legend>
                           
                            <p>
                            	<label for="pendant">Handheld Pendant</label>
                            	<input id="pendant" value="yes" name="pendant" type="checkbox" />
                            </p> 
                             <p>
                            	<label for="swivel">180&deg; Swivel</label>
                            	<input id="swivel" value="yes" name="swivel" type="checkbox" />
                            </p>   
                            <p>
                            	<label for="carpet">Spare Carpet</label>
                            	<select id="carpet" name="carpet">
                            		<option value="none" selected>None</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p> 
                            <p>
                            	<label for="paint">Spare Paint</label>
                            	<select id="paint" name="paint">
                            		<option value="none" selected>None</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p>                                             
                            </fieldset>
						<fieldset class="step">
                            <legend>Confirm</legend>
							<p>
								Please double check that you have filled in all the information correctly.
								A red box beside a field indicates that some field 
								is missing or filled out with invalid data.
							</p>
                            <p class="submit">
                            	<input type="hidden" name="vantype" value="miniVan" />
                            	<input type="hidden" name="placeOrder" value="1" />
                                <input  id="submit" name="submit" type="submit" value="Place Order" />
                            </p>
                        </fieldset>
                    </form>
		</div>
