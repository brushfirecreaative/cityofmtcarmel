<div id="orderForm">
            <h2>Order Form for Full Size Vans</h2>
            <div id="wrapper">
	<div id="steps">
	<form class="transForm" id="formElem" name="formElem" action="process.php" method="post">
                        <fieldset class="step">
                            <legend>Contact Information</legend>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_name qm"></a>
                            	<em class="name tooltip">
								For our records, please tell us the person placing the order. <a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="name"><em class="required">*</em> Ordered By</label> 
                                <input id="name" name="name" type="text"  class="validate[required]" />
                                
                           
                            	
							</p>	
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_po qm"></a>
                            	<em class="po tooltip">
								If you have a specific Purchase Order Number please enter it now.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="po">P.O.</label>
                                <input id="po" name="po" type="text"  class="validate[required]" />
                            </p>
                        </fieldset>
                        <fieldset class="step">
                            <legend>Vehicle Information</legend>
                             <p>
                             	<a href="javascript:void(0)" class="show_hide_vyear qm"></a>
                            	<em class="vyear tooltip">
								The Year of the vehicle.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_year"><em class="required">*</em> Vehicle Year</label>
                                <input id="v_year" name="v_year" type="text"  class="validate[required]" />
                            </p>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_vmake qm"></a>
                            	<em class="vmake tooltip">
								The Make of the vehicle.<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_make"><em class="required">*</em> Make</label>
                                <input id="v_make" name="v_make" type="text"  class="validate[required]" />
                            </p>
                           <p>
                           		<a href="javascript:void(0)" class="show_hide_vmodel qm"></a>
                            	<em class="vmodel tooltip">
								The Model of the vehicle<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="v_model"><em class="required">*</em> Model</label>
                               <input id="v_model" name="v_model" type="text"  class="validate[required]" />
                            </p>
                           
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_driver qm"></a>
                            	<em class="driver tooltip">
								What kind of OEM Driver Seat is currently installed in the vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="driver_sseat"><em class="required">*</em> Type of Driver's Seat</label>
                                <select id="drivers_seat" name="drivers_seat" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="power">OEM Factory Power Seat</option>
                                    <option value="manual">OEM Factory Manual Seat</option>
                                </select>
                            </p>
                            <p>
                            	<a href="javascript:void(0)" class="show_hide_pass qm"></a>
                            	<em class="pass tooltip">
								What kind of OEM Passenger Seat is currently installed in the vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="pass_seat"><em class="required">*</em> Type of Passenger's Seat</label>
                                <select id="pass_seat" name="pass_seat" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="power">OEM Factory Power Seat</option>
                                    <option value="manual">OEM Factory Manual Seat</option>
                                </select>
                            </p>
                             <p>
                             	<a href="javascript:void(0)" class="show_hide_ramp qm"></a>
                            	<em class="ramp tooltip">
								What type of ramp is currently installed in the Vehicle?<a href="javascript:void(0)" class="show_hide_name hide"></a>
								</em>
                                <label for="ramp"><em class="required">*</em> Type of Ramp</label>
                                <select id="ramp" name="ramp" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="floor">In-floor Ramp</option>
                                    <option value="folding">Folding Ramp</option>
                                    <option value="none">None</option>
                                </select>
                            </p>
                            <p>
                                <label for="conversion"><em class="required">*</em> Conversion By</label>
                                <input id="conversion" name="conversion" type="text"  class="validate[required]" />
                            </p>
							<p>
                                <label for="conversion_type"><em class="required">*</em> Conversion Type</label>
                                <input id="conversion_type" name="conversion_type" type="text"  class="validate[required]" />
                            </p>
                             <p>
                                <label for="conversion_year"><em class="required">*</em> Year of Conversion</label>
                                <input id="conversion_year" name="conversion_year" type="text"  class="validate[required]" />
                            </p>
                             <p>
                                <label for="color"><em class="required">*</em> Interior Color</label>
                                <select id="color" name="color" class="validate[required]">
                                	<option value="" selected="selected">Select...</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p>
                        </fieldset>
                        <fieldset class="step">
                            <legend>Leadership Seat Base Options</legend>
                            
							<p>
                            	<label for="l41"><b>L41 Leadership Series:</b> 6 Way for Non-Lowered Floor Full Size Vans</label>
                            	Driver<input type="checkbox" id="l41" name="l41" value="l41-driver"/>
                            	Passenger<input type="checkbox" id="l41" name="l41" value="l41-passenger"/>
								<br />
								<em>
                            		<label for="l41foot">Comfort Safe Footrest</label>
                            		<select id="l41foot" name="l41foot" class="validate[required]">
                            			<option value="" selected="selected">Select...</option>
                            			<option value="Yes">Yes</option>
                            			<option value="No">No</option>
                            		</select>
                            	</em>	
                            </p>
                            <p>
                            	<label for="ls51"><b>LS51 Leadership Series:</b> 6 Way for Lowered Floor Full Size Vans</label>
                            	Driver<input type="checkbox" id="ls51" name="ls51" value="ls51-driver"/>
                            	Passenger<input type="checkbox" id="ls51" name="ls51" value="ls51-passenger"/>
								<br />
								<em>
                            		<label for="ls51cover">Decorative Cover for LS51</label>
                            		<select id="ls51cover" name="ls51cover" class="validate[required]">
                            			<option value="" selected="selected">Select...</option>
                            			<option value="Yes">Yes</option>
                            			<option value="No">No</option>
                            		</select>
                            	</em>	
                            </p> 
							<p>
                            	<label for="l71"><b>L71 Leadership Series:</b> 4 Way for Numerous Non-Lowered Floor Applications</label>
                            	Driver<input type="checkbox" id="l71" name="l71" value="l71-driver"/>
                            	Passenger<input type="checkbox" id="l71" name="l71" value="l71-passenger"/>
								<br />
								<em>
                          			<label for="l71foot">Comfort Safe Footrest</label>
                            		<select id="l71foot" name="l71foot" class="validate[required]">
                            			<option value="" selected="selected">Select...</option>
                            			<option value="Yes">Yes</option>
                            			<option value="No">No</option>
                            		</select>
                         	 	</em>	
							</p>
							<p>
                            	<label for="l73"><b>L73 Leadership Series:</b> 4 Way for Numerous Non-Lowered Floor Applications (Slide &amp; Swivel Only)</label>
                            	Driver<input type="checkbox" id="l73" name="l73" value="l73-driver"/>
                            	Passenger<input type="checkbox" id="l73" name="l73" value="l73-passenger"/>	
                            	<br />
								<em>
                            		<label for="powder">Powder Coat</label>
                            		<select id="powder" name="powder"/>
										<option value="gray" selected>Gray (standard)</option>
                            	    	<option value="tan">Tan (additional fee)</option>
									</select>
                            	</em>
                            </p>
						</fieldset>
						<fieldset class="step">
						<fieldset class="step">
                            <legend>Options &amp; Accessories</legend>
                           
                            <p>
                            	<label for="pendant">Handheld Pendant</label>
                            	<input id="pendant" name="pendant" type="checkbox" />
                            </p> 
                             <p>
                            	<label for="swivel">180&deg; Swivel</label>
                            	<input id="swivel" name="swivel" type="checkbox" />
                            </p>   
                            <p>
                            	<label for="carpet">Spare Carpet</label>
                            	<select id="carpet" name="carpet">
                            		<option value="none" selected>None</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p> 
                            <p>
                            	<label for="paint">Spare Paint</label>
                            	<select id="paint" name="paint">
                            		<option value="none" selected>None</option>
                                    <option value="gray">Gray&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <option value="tan">Tan</option>
                                </select>
                            </p>                                             
                            </fieldset>
						<fieldset class="step">
                            <legend>Confirm</legend>
							<p>
								Please double check that you have filled in all the information correctly.
								A red box beside a field indicates that some field 
								is missing or filled out with invalid data.
							</p>
                            <p class="submit">
                           	 <input type="hidden" name="vantype" value="Fullsize" />
                            	<input type="hidden" name="placeOrder" value="1" />
                                <input  id="submit" name="submit" type="submit" value="Place Order">
                            </p>
                        </fieldset>
                    </form>
		</div>
