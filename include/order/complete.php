<h1 class="success">Please Review Your Order:</h1>

<?
$session->currentorder = $database->getCurrentOrder($session->username);
echo "<h2>Order Number: " .$session->currentorder['order_num']. "</h2>";
echo "<div class='form_wrap'>";
echo "<br />Order Placed By: <strong>" .$session->currentorder['ordered_by']. "</strong></br />";
echo "<h3>Dealer Info:</h3>";
echo "<h4>Shipping Address:</h4>";
echo $session->usershipping['address1']."<br />";
echo $session->usershipping['address2'];
echo $session->usershipping['city']. ", ";
echo $session->usershipping['state']. "&nbsp;";
echo $session->usershipping['zip'];
echo "</div>";
echo "<div class='form_wrap'>";
echo "<h3>Vehicle Information:</h3>";
echo "<table id='review_order'>";
echo "<tr>";
echo "<td class='field'>Van Type:</td>";
echo "<td>" .$session->currentorder['vantype']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Year:</td>";
echo "<td>" .$session->currentorder['v_year']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Make:</td>";
echo "<td>" .$session->currentorder['v_make']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Model:</td>";
echo "<td>" .$session->currentorder['v_model']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Driver's Seat:</td>";
echo "<td>" .$session->currentorder['d_seat']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Passenger's Seat:</td>";
echo "<td>" .$session->currentorder['p_seat']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Ramp Type:</td>";
echo "<td>" .$session->currentorder['ramp_type']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Interior Color:</td>";
echo "<td>" .$session->currentorder['interior_color']. "</td>";
echo "</tr>";
echo "</table>";
echo "</div>";
echo "<div class='form_wrap'>";
echo "<h3>Conversion Information:</h3>";
echo "<table id='review_order'>";
echo "<tr>";
echo "<td class='field'>Conversion By:</td>";
echo "<td>" .$session->currentorder['conversion_by']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Conversion Type:</td>";
echo "<td>" .$session->currentorder['conversion_type']. "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='field'>Conversion Year:</td>";
echo "<td>" .$session->currentorder['c_year']. "</td>";
echo "</tr>";
echo "</table>";
echo "</div>";
echo "<div class='form_wrap'>";
echo "<h3>Seat(s) Ordered:</h3>";
if($session->currentorder['L51'] == "driver"){
		echo "<div class='seat_base'><h2>L51<em>(Driver)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L51.jpg' width='300' />
			</div>";
	}else if($session->currentorder['L51'] == "passenger"){
		echo "<div class='seat_base'><h2>L51<em>(Passenger)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L51.jpg' width='300' />
			</div>";
	}else{
		echo '';
	}
if($session->currentorder['cover'] == "yes"){
		echo "<div class='seat_base'><h2>With L51 Cover</h2>
			<img src='http://bdindependence.com/images/cover.jpg' width='300' />
			</div>";
	}else if($session->currentorder['cover'] == "") {
		echo "";
	}
if($session->currentorder['L53'] == "driver"){
		echo "<div class='seat_base'><h2>L53<em>(Driver)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L53.jpg' width='300' />
			</div>";
	}else if($session->currentorder['L53'] == "passenger"){
		echo "<div class='seat_base'><h2>L53<em>(Passenger)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L53.jpg' width='300' />
			</div>";
	}else{
		echo '';
	}
if($session->currentorder['L75'] == "driver"){
		echo "<div class='seat_base'><h2>L75<em>(Driver)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L75.jpg' width='300' />
			</div>";
	}else if($session->currentorder['L75'] == "passenger"){
		echo "<div class='seat_base'><h2>L75<em>(Passenger)</em></h2>
			<img src='http://bdindependence.com/images/Model%20L75.jpg' width='300' />
			</div>";
	}else{
		echo '';
	}
	
	if($session->currentorder['L41'] == "driver"){
			echo "<div class='seat_base'><h2>L41<em>(Driver)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L41.jpg' width='300' />
				</div>";
		}else if($session->currentorder['L41'] == "passenger"){
			echo "<div class='seat_base'><h2>L41<em>(Passenger)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L41.jpg' width='300' />
				</div>";
		}else{
			echo '';
		}
	if($session->currentorder['L41_foot_rest'] == "Yes"){
			echo "<div class='seat_base'><h2>L41 Foot Rest</h2> 
				</div>";
		}else{
			echo "";
		}
	if($session->currentorder['LS51'] == "driver"){
			echo "<div class='seat_base'><h2>LS51<em>(Driver)</em></h2>
				<img src='http://bdindependence.com/images/Model%20LS51.jpg' width='300' />
				</div>";
		}else if($session->currentorder['LS51'] == "passenger"){
			echo "<div class='seat_base'><h2>LS51<em>(Passenger)</em></h2>
				<img src='http://bdindependence.com/images/Model%20LS51.jpg' width='300' />
				</div>";
		}else{
			echo '';
		}
		if($session->currentorder['LS51_cover'] == "Yes"){
			echo "<div class='seat_base'><h2>LS51 Cover</h2> 
				</div>";
		}else{
			echo "";
		}
	if($session->currentorder['L71'] == "driver"){
			echo "<div class='seat_base'><h2>L71<em>(Driver)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L71.jpg' width='300' />
				</div>";
		}else if($session->currentorder['L71'] == "passenger"){
			echo "<div class='seat_base'><h2>L71<em>(Passenger)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L71.jpg' width='300' />
				</div>";
		}else{
			echo '';
		}
		if($session->currentorder['L71_foot_rest'] == "Yes"){
			echo "<div class='seat_base'><h2>L71 Foot Rest</h2> 
				</div>";
		}else{
			echo "";
		}
		if($session->currentorder['L73'] == "driver"){
			echo "<div class='seat_base'><h2>L73<em>(Driver)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L73.jpg' width='300' />
				</div>";
		}else if($session->currentorder['L73'] == "passenger"){
			echo "<div class='seat_base'><h2>L73<em>(Passenger)</em></h2>
				<img src='http://bdindependence.com/images/Model%20L73.jpg' width='300' />
				</div>";
		}else{
			echo '';
		}


?>