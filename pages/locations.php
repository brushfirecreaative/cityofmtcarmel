<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 class="cushycms" id="PageHeading">Location</h2>

<p class="cushycms" id="OpeningParagrph">Mount Carmel is centrally located to the Midwest&#39;s largest cities. This allows your business to quickly get its shipments where they need to go, regardless of where your business takes you. The chart below indicates our distance from major cities. <a href="/assets/road-to-mount-carmel.html">Get Direction to Mount Carmel</a></p>

<p><img alt="Location" class="right" src="/assets/images/location_map.jpg" width="700" /></p>

<table id="location_table">
	<tbody>
		<tr>
			<td>City</td>
			<td>Miles</td>
		</tr>
		<tr>
			<td>St. Louis, MO</td>
			<td>120</td>
		</tr>
		<tr>
			<td>Indianapolis, IN</td>
			<td>134</td>
		</tr>
		<tr>
			<td>Chicago, IL</td>
			<td>250</td>
		</tr>
		<tr>
			<td>Louisville, KY</td>
			<td>132</td>
		</tr>
		<tr>
			<td>Memphis, TN</td>
			<td>324</td>
		</tr>
		<tr>
			<td>Atlanta, GA</td>
			<td>500</td>
		</tr>
		<tr>
			<td>New Orleans, LA</td>
			<td>720</td>
		</tr>
	</tbody>
</table>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>