<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 class="cushycms" id="PageHeading">Where to stay when visiting Mount Carmel</h2>
<iframe frameborder="0" height="400" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps?q=62863+lodging&amp;hl=en&amp;client=firefox-a&amp;ie=UTF8&amp;hq=lodging&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;t=m&amp;vpsrc=6&amp;fll=38.413047,-87.780504&amp;fspn=0.038939,0.090895&amp;st=115968771510351694523&amp;rq=1&amp;ev=zi&amp;split=1&amp;sll=38.412183,-87.788717&amp;sspn=0.025116,0.06469&amp;ll=38.413181,-87.762308&amp;spn=0.026901,0.082312&amp;z=14&amp;output=embed" width="960"></iframe><br />
<small><a href="http://maps.google.com/maps?q=62863+lodging&amp;hl=en&amp;client=firefox-a&amp;ie=UTF8&amp;hq=lodging&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;t=m&amp;vpsrc=6&amp;fll=38.413047,-87.780504&amp;fspn=0.038939,0.090895&amp;st=115968771510351694523&amp;rq=1&amp;ev=zi&amp;split=1&amp;sll=38.412183,-87.788717&amp;sspn=0.025116,0.06469&amp;ll=38.413181,-87.762308&amp;spn=0.026901,0.082312&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>

<div class="cushycms" id="Super8">
<h3><a href="http://www.super8.com/Super8/control/Booking/modify_dates?areaCode=&amp;brandCode=DI,HJ,KG,RA,SE,TL,WG,BU,WY,BH,MT&amp;searchWithinMiles=50&amp;areaType=1&amp;destination=Mt.%20Carmel&amp;stateName=Illinois&amp;state=IL&amp;countryName=United%20States&amp;country=US&amp;checkInDate=&amp;numberAdults=1&amp;numberRooms=1&amp;checkOutDate=&amp;numberChildren=0&amp;numberBigChildren=0&amp;useWRPoints=false&amp;variant=&amp;id=14052&amp;propBrandId=SE&amp;force_nostay=false&amp;tab=tab1">Super 8</a></h3>
<small><a href="http://maps.google.com/maps?q=62863+lodging&amp;hl=en&amp;client=firefox-a&amp;ie=UTF8&amp;hq=lodging&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;t=m&amp;vpsrc=6&amp;fll=38.413047,-87.780504&amp;fspn=0.038939,0.090895&amp;st=115968771510351694523&amp;rq=1&amp;ev=zi&amp;split=1&amp;sll=38.412183,-87.788717&amp;sspn=0.025116,0.06469&amp;ll=38.413181,-87.762308&amp;spn=0.026901,0.082312&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>

<div class="cushycms" id="Super8">
<h3><a href="http://www.super8.com/Super8/control/Booking/modify_dates?areaCode=&amp;brandCode=DI,HJ,KG,RA,SE,TL,WG,BU,WY,BH,MT&amp;searchWithinMiles=50&amp;areaType=1&amp;destination=Mt.%20Carmel&amp;stateName=Illinois&amp;state=IL&amp;countryName=United%20States&amp;country=US&amp;checkInDate=&amp;numberAdults=1&amp;numberRooms=1&amp;checkOutDate=&amp;numberChildren=0&amp;numberBigChildren=0&amp;useWRPoints=false&amp;variant=&amp;id=14052&amp;propBrandId=SE&amp;force_nostay=false&amp;tab=tab1">Super 8</a></h3>

<p>937 Enterprise Lane, Mount Carmel<br />
1-888-288-5081</p>
<img alt="super8" class="right" src="/assets/images/super8.jpg" />
<p>The Super 8 Mt Carmel is located near the Illinois/Indiana border on the banks of the Wabash River. The Wabash Valley College is just 3 miles from the hotel. For recreation enjoy nearby Beall Woods, Gibson Lake, or the Mt. Carmel Golf Course. We are located just 15 miles from the Toyota Plant in nearby Princeton.</p>

<h4>Amenities</h4>

<ul class="amenities">
	<li>High Speed Wireless Internet</li>
	<li>Complimentary SuperStart Breakfast</li>
	<li>Free Local Phone</li>
	<li>Guest Laundry</li>
	<li>Interior Corridor</li>
	<li>RV or Truck Parking</li>
	<li>27 inch TV</li>
	<li>HBO</li>
</ul>

<ul class="amenities2">
	<li>Business Desk and Ottoman in King Size Rooms</li>
	<li>Coffee Maker</li>
	<li>Hair Dryer</li>
	<li>Microfridge</li>
	<li>Rollaways $10.00</li>
	<li>Free Cribs</li>
	<li>Wirlpool Suites Available</li>
</ul>
</div>

<div class="clear">&nbsp;</div>

<div class="h_divider">&nbsp;</div>

<div class="cushycms">
<h3><a href="http://swiftsanctuary.rentals/" target="_blank">Swift Sanctuary</a></h3>

<h3><span style="font-size: 13px; line-height: 1.6em;">226 E 5th Street Mount Carmel, Il 62863</span></h3>

<p>618-262-5797</p>

<h3>Shamrock Motel</h3>

<p>1303 North Cherry St. Mount Carmel, IL 62863<br />
618-262-4169</p>
</div>

<div class="cushycms">
<h3>Economy Inn Express</h3>

<p>511 North Market Street, Mount Carmel<br />
618-262-8000</p>
</div>

<div class="cushycms">
<h3>Town &amp; Country Motel</h3>

<p>1515 West 3rd Street, Mount Carmel<br />
618-262-4171</p>
</div>
</div>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>