<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 class="cushycms" id="PageHeading">Attractions in Mount Carmel, IL</h2>

<p><br />
&nbsp;</p>

<div><img alt="Wabash county museum" class="right cushycms" src="/assets/images/meseum.jpg" />
<h4 class="cushycms"><a href="http://www.museum.wabash.il.us/">Wabash County Museum</a></h4>

<p class="cushycms">The Wabash County Museum District was formed on January 7, 1991, with a board of 5 appointed by the Wabash County Commissioners to serve as Museum District commissioners. In June of 2005 the current building was purchased at 320 Market Street, and with minor renovations gave the museum 12,000 square feet of exhibit space, as well as secure archive space and a research library. Permanent exhibits include a &quot;Tribute to Brace Beemer, Radio&#39;s Lone Ranger&quot;, the &quot;Oil Boom Era&quot;, and &quot;Churches Around Wabash County&quot;. Other exhibits are designed and opened on a regular basis. Weekly hours are from 2pm to 5pm on Sundays, Tuesdays and Thursdays.<br />
<a href="http://www.museum.wabash.il.us/">More Information on the Wabash County Chamber</a></p>

<div class="clear">&nbsp;</div>
<img alt="Beall Woods" class="right cushycms" src="/assets/images/beall-woods.jpg" />
<h4 class="cushycms"><a href="http://www.dnr.state.il.us/lands/landmgt/parks/r5/beall.htm">Beall Woods</a></h4>

<p class="cushycms">Located on the banks of the Wabash River in southeastern Illinois, Beall Woods attracts visitors from around the world wanting a glimpse of one of the few remaining tracts of virgin timber east of the Mississippi River where one can see trees 120 feet tall and over 3 feet in diameter. Besides hiking, Beall Woods also offers camping, picnicking, and fishing to the visitor who wants to spend time in a quiet, relaxing setting.<br />
<a href="http://www.dnr.state.il.us/lands/landmgt/parks/r5/beall.htm">More Information on Beall Woods</a></p>

<div class="clear">&nbsp;</div>
<img alt="Wabash River" class="right cushycms" src="/assets/images/wabash-river.jpg" />
<h4 class="cushycms"><a href="http://www.wabashriver.us/access_points/mt_carmel_north_pas/index.htm">Wabash River</a></h4>

<p class="cushycms">Individuals can enjoy boating on the Wabash via a boat ramp at East 4th Street in Mt. Carmel. <a href="http://www.wabashriver.us/access_points/mt_carmel_north_pas/index.htm">More Information on Wabash River</a></p>

<div class="clear">&nbsp;</div>
<img alt="RibberFest" class="right cushycms" src="/assets/images/ribberfest.jpg" />
<h4 class="cushycms"><a href="http://www.wabashribberfestbbq.com/">Ribberfest Champion BBQ</a></h4>

<p class="cushycms">The smell of World Champion BBQ drifts through the air during the first weekend in September in Wabash county as the city of Mount Carmel hosts Ribberfest, a preliminary competition for the national &quot;Kansas City&quot; cook-off. More than a dozen teams from throughout the nation compete for the title of &quot;Best BBQ.&quot; Entertainment, car shows and much more await visitors. <a href="http://www.wabashribberfestbbq.com/">More Information on Ribber Fest</a></p>

<div class="clear">&nbsp;</div>

<p class="cushycms">For more attractions in Wabash County and throughout Southeastern Illinois, check out <a href="http://www.southeastillinois.com/">SouthEastern Illinois Tourism</a></p>
</div>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>