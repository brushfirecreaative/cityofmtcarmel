<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">

<h2>Available Programs for Business</h2>
<img alt="Buisness Assistants" class="right" src="/assets/images/shapeimage_2.png" width="399" />
<p class="cushycms"><a href="http://mcaea.com/" target="_blank"><img alt="" src="/assets/city_files/pdfs/area%20economic%20alliance.png" style="width: 216px; height: 135px;" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://mcaea.com/" target="_blank">Executive Director Ben Ross</a></p>

<p class="cushycms">&nbsp;</p>

<h4>Business Infrastructure Assistance Program</h4>
The City of Mt. Carmel can supply TIF funds to industrial firms located or expanding in a TIF to cover up to 50% of the cost of approved infrastructure improvements. These improvements can involve all elements of public way infrastructure. Eligible projects include but are not limited to: light pole and fire hydrant relocation; hazardous vaulted sidewalk elimination; traffic signalization; water and sewer line improvements; utility removal and relocation; sidewalk, curb, gutter and street improvements; and landscaping.

<p>&nbsp;</p>

<p class="cushycms">&nbsp;</p>

<h4>Small Business Improvement Fund</h4>
This program provides rebates of up to 50% of the cost of improvements to an industrial building. The maximum rebate per building is $50,000. Funding is provided for facade renovation, lighting, signs, graphics, windows, doors, floors, roofs, tuck pointing, HVAC, ADA, security, expansion and parking. To be eligible, companies must be located in one of the TIF-funded industrial corridors. Call the Economic Development Office to find out if you are eligible.

<p>&nbsp;</p>

<p class="cushycms">&nbsp;</p>

<h4>&nbsp;</h4>

<h4>Training</h4>
We bring training experts from the Illinois Eastern Community College District to your company to assess your training needs, customize a curriculum and set up training at your facility, depending on your needs. Training ranges from OSHA to ESL to supervisory and industrial skills.

<h5>Training Subsidies</h5>
TIFworks: Companies located within, expanding or relocating to an eligible TIF district are eligible for a training reimbursement of up to 100 percent of an employer&#39;s cost of developing or purchasing customized training programs.

<h5>Employment Training Investment Program (ETIP)</h5>
Funded through the Illinois Department of Commerce and Economic Opportunity (DCEO) provides a rebate of up to 50 percent of training expenses for full-time workers.

<p>&nbsp;</p>

<p class="cushycms">&nbsp;</p>

<h4>Tax Credits</h4>

<h5>Illinois Deptartment of Employment Security (IDES)</h5>
The IDES administers State of Illinois tax credits including Work Opportunity and Welfare-to-Work tax credits, and tax credits for ex-felons, veterans, youth and others.

<h5>Job Tax Credits</h5>
The enterprise zone tax credit 35 ILCS 5/201 offers employers a tax credit on their Illinois income taxes for hiring individuals who are certified as economically disadvantaged or as dislocated workers working a minimum of 180 consecutive days for 30 hours or more per week. Call the Economic Development Office for certification information.

<h5>Enterprise Zone Property Tax Abatement</h5>
A 5 year 100% property tax abatement on improved property in an Enterprise Zone.

<h5>Enterprise Zone State Sales Tax Deduction</h5>
Available on permanent building materials &amp; machinery in an Enterprise Zone.

<h5>Enterprise Zone Income Tax Deduction</h5>
Financial Institutions are eligible for a special deduction on their Illinois Corporate Income Tax Return. Such institutions may deduct from their taxable income an amount equal to the interest received from a loan for development in an Enterprise Zone.

<p>&nbsp;</p>

<p class="cushycms">&nbsp;</p>

<h4>Revolving Loan Fund</h4>
A low-interest loan program that is based on job creation with a lending rate of 10,000 per full-time job created or retained. <a href="http://cityofmtcarmel.com/doing_business/revolving_loan_fund.php">more...</a>

<h4>More Resources...</h4>
<a href="http://www.commerce.state.il.us/dceo/Bureaus/Business_Development/Grants" target="_blank">State Grants</a><br />
<a href="http://www.commerce.state.il.us/dceo/Bureaus/Business_Development/Tax+Assistance/" target="_blank">State Tax Incentives</a><br />
<a href="http://www.commerce.state.il.us/dceo/Bureaus/Business_Development/Loan+Programs/" target="_blank">State Loan Programs</a><br />
<a href="http://www.grants.gov/" target="_blank">Federal Grant Finder</a>

<p>&nbsp;</p>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>