<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
				<h2 class="cushycms">Recreation in Mount Carmel, Illinois</h2>
				<div>
				<h4>The Wabash River</h4>
				<img alt="Wabash Canoe" class="right cushycms" src="http://cityofmtcarmel.com/city_files/wabash%20101.jpg" width="300" />
				<p class="cushycms">Whether you visit Mount Carmel for a day, or stay for a lifetime, you&#39;re sure to find something fun to do. Our city is nestled along the banks of the Wabash and White Rivers, providing many opportunities for boating and fishing.</p>

				<div class="clear">&nbsp;</div>

				<h4 class="cushycms">Mount Carmel Municipal Golf Course</h4>
				<img alt="golf1" class="right cushycms" src="http://cityofmtcarmel.com/images/golf1.jpg" width="400" />
				<p class="cushycms">If you&#39;d like to spend your time going over the water in the air instead of on top of it, you might tee off at one of the finest golf courses in the tri-state. <a href="http://www.mtcarmelgolfcourse.com/" target="_blank">Mount Carmel&#39;s 18 hole municipal course</a> draws golfers of all handicaps from a wide area.</p>

				<div class="clear">&nbsp;</div>

				<h4 class="cushycms">Mount Carmel City Pool</h4>
				<img alt="city_pool" class="right cushycms" height="300" src="http://cityofmtcarmel.com/images/city_pool.jpg" width="400" />
				<p class="cushycms">If a slightly less strenuous afternoon is on the schedule, you could be in the water, cooling off on a hot day at one of the few circular, sloping, family friendly municipal swimming pools in the country. For the more adventurous, high dives and slides take you directly to the deeper waters.</p>

				<div class="clear">&nbsp;</div>

				<h4 class="cushycms">Beall Woods State Nature Reserve</h4>
				<img alt="Beall Woods" class="right cushycms" src="http://cityofmtcarmel.com/city_files/beall.2.jpg" width="200" />
				<p class="cushycms">Dry land more to your liking? Go outdoors at <a href="http://www.dnr.state.il.us/lands/landmgt/parks/r5/beall.htm" target="_blank">Beall Woods State Nature Reserve</a> with acres of woodland and over 300 native species of trees, carpets of wildflowers, and wild animals in their natural habitat. You can also take the family for a picnic at either the Lions Club City Park, with its great children&#39;s play areas, or Froman Park, where you can enjoy the shade of lush trees, or maybe even catch a baseball game, played at the adjacent Dee White Memorial Field.</p>
				</div>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>