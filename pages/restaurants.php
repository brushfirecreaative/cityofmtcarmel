<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h1 class="cushycms" id="PageHeading">Where to eat in Mount Carmel</h1>
&nbsp;

<h2 class="cushycms">Local Restraunts</h2>
&nbsp;

<div class="cushycms" id="LocalRestraunts">
<h4>Big John&#39;s Lunch Box</h4>

<p>224 McDowell Street, Mount Carmel, IL<br />
618-263-3272</p>

<h4>Hogg Heaven BBQ</h4>

<p>411 West 9th Street, Mount Carmel, IL<br />
618-262-7779</p>

<h4>Kellyo&#39;s Pizza</h4>

<p>702 North Market Street, Mount Carmel, IL<br />
618-262-8333</p>

<h4>Lin&#39;s King Buffet &amp; Chinese</h4>

<p>201 North Walnut Street, Mount Carmel, IL<br />
618-262-2532</p>

<h4>Little Italy&#39;s Pizza</h4>

<p>502 North Walnut Street, Mount Carmel, IL<br />
618-262-4121</p>

<h4>Nostalgia</h4>

<p>118 West 5th Street, Mount Carmel, IL<br />
618-262-8400</p>

<h4>Penny&#39;s Pizza</h4>

<p>620 West 9th Street, Mount Carmel, IL<br />
618-262-7300</p>

<h4>Taco Tierra</h4>

<p>729 North Market Street, Mount Carmel, IL<br />
618-262-8226</p>

<h4>Tequila&#39;s Mexican Restaurant</h4>

<p>113 West 9th St. Mount Carmel, IL<br />
618-262-5275</p>

<h4>Twin Rivers Restaurant</h4>

<p>1120 Hydraulic Avenue, Mount Carmel, IL<br />
618-450-2274</p>
</div>
&nbsp;

<h2 class="cushycms">National Chains</h2>
&nbsp;

<div class="cushycms">
<h4>Burger King</h4>

<h4>Long John Silvers</h4>

<h4>McDonald&#39;s</h4>

<h4>Pizza Hut</h4>

<h4>Subway</h4>
</div>
<br />
<iframe width="960" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Restaurants+near+Mount+Carmel,+IL&amp;aq=0&amp;sll=38.413114,-87.772522&amp;sspn=0.040553,0.090895&amp;vpsrc=6&amp;ie=UTF8&amp;hq=Restaurants&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;t=m&amp;fll=38.413786,-87.763424&amp;fspn=0.038939,0.090895&amp;st=115968771510351694523&amp;rq=1&amp;ev=zo&amp;split=1&amp;ll=38.41271,-87.753553&amp;spn=0.026901,0.082312&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Restaurants+near+Mount+Carmel,+IL&amp;aq=0&amp;sll=38.413114,-87.772522&amp;sspn=0.040553,0.090895&amp;vpsrc=6&amp;ie=UTF8&amp;hq=Restaurants&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;t=m&amp;fll=38.413786,-87.763424&amp;fspn=0.038939,0.090895&amp;st=115968771510351694523&amp;rq=1&amp;ev=zo&amp;split=1&amp;ll=38.41271,-87.753553&amp;spn=0.026901,0.082312&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>