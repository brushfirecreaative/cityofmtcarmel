<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2>Mount Carmel Senior Citizens Center</h2>
			<img alt="Senior Citizen Center" class="right cushycms" src="/assets/images/senior-center.jpg" />
			<p class="cushycms">&nbsp;</p>

			<p>The Mount Carmel Senior Citizens Center serves as a gathering place for the &quot;60 and over&quot; crowd from Mount Carmel and Wabash County but, we love to have visitors from surrounding counties. Senior residents visit at the center on a regular basis for coffee, meals, entertainment, and a variety of other activities. Lunch is served every day from 11:30 A.M. to 12:30 P.M. Carry out meals are available to pick up from 11:00 A.M. till 11:15 A.M. and again after all in house have been served.</p>

			<p>&nbsp;</p>

			<p class="cushycms">&nbsp;</p>

			<p>In addition to the daily lunch, activities at the Center include bingo every Thursday after lunch, blood pressure checks once a week, ceramics class in the basement every Tuesday. Senior pool players take advantage of the 3 pool tables located in the basement(which is handicapped accessible). We also have a variety of exercise equipment for seniors to use.</p>

			<p>The RIDES MASS TRANSIT has taken over transportation for the Senior Center. You can request a card through us, and as long as you come up and eat once a month, the bus card will be free of charge. The number to RIDES is toll free 1-877-667-6118.</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p class="cushycms">&nbsp;</p>

			<p>The center is located at 115 East 3rd Street in the historic downtown district area. For information, contact the center at (618) 262-7403.</p>

			<p>&nbsp;</p>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>