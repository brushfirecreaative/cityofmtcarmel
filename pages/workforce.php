<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 class="cushycms" id="PageHeader">Workforce in Mount Carmel, Illinois</h2>

<p><img alt="Workforce" class="right no_border cushycms" src="/assets/images/Workforce.jpg" /><a href="http://mcaea.com/" target="_blank"><img alt="" src="/assets/city_files/pdfs/area%20economic%20alliance.png" style="width: 216px; height: 135px;" /></a></p>

<p>For more information contact <a href="http://mcaea.com/" target="_blank">Executive Director Ben Ross</a>&nbsp;or call 618-262-5446</p>

<p class="cushycms">The population within a 30 mile radius of Mount Carmel is approximately 43,000. According to the Illinois Department of Employment Security, and Indiana Workforce Development, 11,161 people are unemployed as of 2007. These numbers insure a deep and consistent source of capable workers to fill the needs of your growing business.</p>

<p class="cushycms">The US Census showed the average household income in Wabash County to be $31,715. The chart below shows the average wage for workers in various occupations.</p>

<table id="workforce_table">
	<tbody>
		<tr>
			<td>Industry Sector</td>
			<td>Total Employees</td>
			<td>Average Monthly Wage</td>
			<td>Average New Hire Monthly Wage</td>
		</tr>
		<tr>
			<td>Education</td>
			<td>553</td>
			<td>$1,940</td>
			<td>N/A</td>
		</tr>
		<tr>
			<td>Retail trade</td>
			<td>482</td>
			<td>$1,661</td>
			<td>$1,023</td>
		</tr>
		<tr>
			<td>Health Care</td>
			<td>482</td>
			<td>$2500</td>
			<td>$1600</td>
		</tr>
		<tr>
			<td>Construction</td>
			<td>482</td>
			<td>$3400</td>
			<td>$3200</td>
		</tr>
		<tr>
			<td>Mining</td>
			<td>482</td>
			<td>$3800</td>
			<td>$3100</td>
		</tr>
		<tr>
			<td>Manufacturing</td>
			<td>482</td>
			<td>$3200</td>
			<td>$1600</td>
		</tr>
	</tbody>
</table>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>