<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<p>City Council Meetings</p>

<p><a href="/assets/city_files/pdfs/2015%20COUNCILHOLIDAYS.pdf" target="_blank">2016&nbsp;COUNCIL MEETINGS &amp; HOLIDAYS</a></p>

<h3>Next Meeting:</h3>

<h4>10-10-16&nbsp;@ 5:00 pm</h4>

<p><br />
*Minutes will be posted once they are approved by council vote. (Council votes to approve minutes from pervious meeting at each meeting)</p>

<h3>Meeting Minutes</h3>

<h5>2016</h5>

<ul>
	<li><a href="/assets/city_files/pdfs/09-26-16.pdf" target="_blank">09-26-16</a></li>
	<li><a href="/assets/city_files/pdfs/09-12-16.pdf" target="_blank">09-12-16</a></li>
	<li><a href="/assets/city_files/pdfs/08-29-16.pdf" target="_blank">08-29-16</a></li>
	<li><a href="/assets/city_files/pdfs/08-15-16.pdf" target="_blank">08-15-16</a></li>
	<li><a href="/assets/city_files/pdfs/08-01-16.pdf" target="_blank">08-01-16</a></li>
	<li><a href="/assets/city_files/pdfs/07-18-16.pdf" target="_blank">07-18-16</a></li>
	<li><a href="/assets/city_files/pdfs/07-05-16.pdf" target="_blank">07-05-16</a></li>
	<li><a href="/assets/city_files/pdfs/06-20-16.pdf" target="_blank">06-20-16</a></li>
	<li><a href="/assets/city_files/pdfs/06-06-16.pdf" target="_blank">06-06-16</a></li>
	<li><a href="/assets/city_files/pdfs/05-23-16.pdf" target="_blank">05-23-16</a></li>
	<li><a href="/assets/city_files/pdfs/05-09-16.pdf" target="_blank">05-09-16</a></li>
	<li><a href="/assets/city_files/pdfs/04-25-16.pdf" target="_blank">04-25-16</a></li>
	<li><a href="/assets/city_files/pdfs/04-11-16.pdf" target="_blank">04-11-16</a></li>
	<li><a href="/assets/city_files/pdfs/03-28-16.pdf" target="_blank">03-28-16</a></li>
	<li><a href="/assets/city_files/pdfs/03-14-16.pdf" target="_blank">03-14-16</a></li>
	<li><a href="/assets/city_files/pdfs/02-29-16.pdf" target="_blank">02-29-16</a></li>
	<li><a href="/assets/city_files/pdfs/02-15-16.pdf" target="_blank">02-15-16</a></li>
	<li><a href="/assets/city_files/pdfs/02-01-16.pdf" target="_blank">02-01-16</a></li>
	<li><a href="/assets/city_files/pdfs/01-18-16%20.pdf" target="_blank">01-18-16</a></li>
	<li><a href="/assets/city_files/pdfs/01-04-16.pdf" target="_blank">01-04-16</a></li>
</ul>

<h5>2015</h5>

<ul>
	<li><a href="/assets/city_files/pdfs/12-21-15.pdf" target="_blank">12-21-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/12-07-15..pdf" target="_blank">12-07-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-23-15.pdf" target="_blank">11-23-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-09-15.pdf" target="_blank">11-09-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-26-15.pdf" target="_blank">10-26-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-12-15%20Minutes.doc" target="_blank">10-12-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-28-15.pdf" target="_blank">09-28-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-14-15.pdf" target="_blank">09-14-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-31-15.pdf" target="_blank">08-31-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-17-15%20.pdf" target="_blank">08-17-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-10-15.pdf" target="_blank">08-10-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-20-15.pdf" target="_blank">07-20-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-06-15.pdf" target="_blank">07-06-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/06-22-15.pdf" target="_blank">06-22-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/06-08-15%20.pdf" target="_blank">06-08-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-26-15.pdf">05-26-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-11-15.pdf">05-11-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-27-15.pdf">04-27-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-13-15.pdf" target="_blank">04-13-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-30-15.pdf" target="_blank">03-30-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-16-15.pdf" target="_blank">03-16-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-02-15.pdf" target="_blank">03-02-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-16-15.pdf" target="_blank">02-16-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-02-15.pdf" target="_blank">02-02-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-19-15..pdf" target="_blank">1-19-15.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-05-15%20.pdf" target="_blank">1-05-15.pdf</a></li>
</ul>

<p>2014</p>

<ul>
	<li><a href="/assets/city_files/pdfs/12-22-14.pdf" target="_blank">12-22-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/12-08-14.pdf" target="_blank">12-08-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-24-14.pdf" target="_blank">11-24-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-10-14.pdf" target="_blank">11-10-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-27-14.pdf" target="_blank">10-27-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-13-14.pdf" target="_blank">10-13-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-29-14.pdf" target="_blank">09-29-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-15-14.pdf" target="_blank">09-15-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-02-14.pdf" target="_blank">09-02-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-18-14.pdf" target="_blank">08-18-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-04-14.pdf" target="_blank">08-04-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-21-14%20Minutes.pdf" target="_blank">07-21-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-07-14.pdf" target="_blank">07-07-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/06-23-14%20.pdf" target="_blank">06-23-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/06-09-14.pdf" target="_blank">06-09-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-27-14.pdf" target="_blank">05-27-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-12-14.pdf" target="_blank">05-12-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-28-14.pdf" target="_blank">04-28-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-14-14.pdf" target="_blank">04-14-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-31-14.pdf" target="_blank">03-31-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-17-14.pdf" target="_blank">03-17-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-03-14.pdf" target="_blank">03-03-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-17-14.pdf" target="_blank">02-17-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-03-14.pdf" target="_blank">02-03-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-20-14.pdf" target="_blank">01-20-14.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-06-14.pdf" target="_blank">01-06-14.pdf</a></li>
</ul>

<h5>2013</h5>

<ul>
	<li><a href="/assets/city_files/pdfs/12-23-13.pdf" target="_blank">12-23-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/12-09-13.pdf" target="_blank">12-9-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-25-13%20.pdf" target="_blank">11-25-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/11-12-13.pdf" target="_blank">11-12-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-28-13.pdf" target="_blank">10-28-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/10-14-13.pdf" target="_blank">10-14-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-30-13.pdf" target="_blank">09-30-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/09-16-13.pdf" target="_blank">09-16-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-19-13.pdf" target="_blank">08-19-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/08-05-13.pdf" target="_blank">08-05-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-22-13.pdf" target="_blank">07-22-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/07-08-13.pdf" target="_blank">07-08-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/06-10-13.pdf" target="_blank">06-10-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-28-13.pdf" target="_blank">05-28-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/05-13-13%20.pdf" target="_blank">05-13-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-29-13.pdf" target="_blank">04-29-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-15-13.pdf" target="_blank">04-15-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/04-01-13.pdf" target="_blank">04-01-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-18-13.pdf" target="_blank">03-18-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/03-04-13.pdf">03-03-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-18-13.pdf" target="_blank">02-18-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/02-04-13.pdf" target="_blank">02-04-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-21-13.pdf" target="_blank">01-21-13.pdf</a></li>
	<li><a href="/assets/city_files/pdfs/01-07-13.pdf" target="_blank">01-07-13.pdf</a></li>
</ul>

<h5>2012</h5>

<div id="">
<ul>
	<li><a href="" target="_blank">12-26-12.pdf</a></li>
	<li><a href="" target="_blank">12-17-12.pdf</a></li>
	<li><a href="" target="_blank">12-10-12.pdf</a></li>
	<li><a href="" target="_blank">11-26-12.pdf</a></li>
	<li><a href="" target="_blank">11-13-12.pdf</a></li>
	<li><a href="" target="_blank">10-29-12.pdf</a></li>
	<li><a href="" target="_blank">10-15-12.pdf</a></li>
	<li><a href="" target="_blank">10-01-12.pdf</a></li>
	<li><a href="" target="_blank">09-17-12.pdf</a></li>
	<li><a href="" target="_blank">09-04-12 .pdf</a></li>
	<li><a href="" target="_blank">08-20-12.pdf</a></li>
	<li><a href="" target="_blank">08-06-12.pdf</a></li>
	<li><a href="" target="_blank">07-23-12.pdf</a></li>
	<li><a href="" target="_blank">07-09-12.pdf</a></li>
	<li><a href="" target="_blank">06-25-12.pdf</a></li>
	<li><a href="" target="_blank">06-11-12.pdf</a></li>
	<li><a href="" target="_blank">05-29-12.pdf</a></li>
	<li><a href="" target="_blank">05-14-12.pdf</a></li>
	<li><a href="" target="_blank">04-30-12.pdf</a></li>
	<li><a href="" target="_blank">04-16-12.pdf</a></li>
	<li><a href="" target="_blank">04-02-12.pdf</a></li>
	<li><a href="" target="_blank">03-19-12.pdf</a></li>
	<li><a href="" target="_blank">03-05-12.pdf</a></li>
	<li><a href="" target="_blank">02-20-12.pdf</a></li>
	<li><a href="" target="_blank">02-06-12.pdf</a></li>
	<li><a href="" target="_blank">01-23-12.pdf</a></li>
	<li><a href="" target="_blank">01-09-12.pdf</a></li>
</ul>

<ul>
	<li><a href="" target="_blank">12-27-11.pdf</a></li>
	<li><a href="" target="_blank">12-15-11.pdf</a></li>
	<li><a href="" target="_blank">12-12-11 .pdf</a></li>
	<li><a href="" target="_blank">12-07-11.pdf</a></li>
	<li><a href="" target="_blank">11-28-11.pdf</a></li>
	<li><a href="" target="_blank">11-14-11.pdf</a></li>
	<li><a href="" target="_blank">10-31-11.pdf</a></li>
	<li><a href="" target="_blank">10-17-11.pdf</a></li>
	<li><a href="" target="_blank">10-03-11.pdf</a></li>
	<li><a href="" target="_blank">09-27-11.pdf</a></li>
	<li><a href="" target="_blank">09-19-11.pdf</a></li>
	<li><a href="" target="_blank">09-06-11.pdf</a></li>
	<li><a href="" target="_blank">08-29-11.pdf</a></li>
	<li><a href="" target="_blank">08-22-11.pdf</a></li>
	<li><a href="" target="_blank">08-08-11.pdf</a></li>
	<li><a href="" target="_blank">07-25-11.pdf</a></li>
	<li><a href="" target="_blank">07-11-11.pdf</a></li>
	<li><a href="" target="_blank">06-27-11.pdf</a></li>
	<li><a href="" target="_blank">06-13-11.pdf</a></li>
	<li><a href="" target="_blank">05-31-11.pdf</a></li>
	<li><a href="" target="_blank">05-16-11.pdf</a></li>
	<li><a href="" target="_blank">05-03-11.pdf</a></li>
	<li><a href="" target="_blank">04-18-11.pdf</a></li>
	<li><a href="" target="_blank">04-04-11.pdf</a></li>
	<li><a href="" target="_blank">03-22-11.pdf</a></li>
	<li><a href="" target="_blank">03-07-11.pdf</a></li>
	<li><a href="" target="_blank">02-21-11.pdf</a></li>
	<li><a href="" target="_blank">02-07-11.pdf</a></li>
	<li><a href="" target="_blank">01-24-11.pdf</a></li>
	<li><a href="" target="_blank">01-10-11.pdf</a></li>
</ul>

<p>&nbsp;</p>

<h5><a href="">Archived Minutes</a></h5>
</div>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>