<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2>The Road to Mount Carmel</h2>
<iframe frameborder="0" height="500" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Mount+Carmel,+IL&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;ll=38.414997,-87.759218&amp;spn=0.033625,0.082312&amp;z=14&amp;iwloc=A&amp;output=embed" width="960"></iframe><br />
<small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Mount+Carmel,+IL&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;ll=38.414997,-87.759218&amp;spn=0.033625,0.082312&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small> <small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Mount+Carmel,+IL&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=Mt+Carmel,+Wabash,+Illinois&amp;ll=38.414997,-87.759218&amp;spn=0.033625,0.082312&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>

<p class="cushycms">Use the map above to plan your trip to Mount Carmel. Click &quot;Directions&quot; to get directions from anywhere. Or search for places you might want to visit by clicking &quot;Search Nearby&quot;</p>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>