<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">

			<h2>City Services</h2>

<ul class="idTabs">
	<li><a href="#water">Water &amp; Sewage</a></li>
	<li><a href="#garbage">Garbage</a></li>
	<li><a href="#apps">Applications &amp; Regulations</a></li>
</ul>

<div class="clear spacer">&nbsp;</div>

<div id="water"><img alt="water" class="right cushycms" src="/assets/images/WaterFaucet.jpg" width="300" />
<div class="cushycms">
<p><font size="4">Your water bill is&nbsp; going <font color="#008000">green</font>! </font></p>

<p><span class="593020620-30092009"><font face="Arial" size="2">The City of Mount Carmel&nbsp;continues to look for ways to&nbsp;be environmentally responsible. One way to save paper, postage&nbsp;and expedite water billing information is for water customers to <b>receive Utility Billing statements electronically</b>. To help save paper and time, residents can request monthly statements as an email attachment. To receive the monthly bill via email, please send your account number or address by clicking&nbsp;&nbsp;<a href="mailto:mgidcumb@cityofmtcarmel.com?subject=Go%20Green">GO Green</a></font></span></p>

<p><b><font face="Arial" size="2">Payment Options:</font></b></p>

<p><a href="https://www.courtmoney.com/makepayment/payment.php?testID=2411" style="line-height: 1.6em;" target="_blank"><img alt="" src="/assets/city_files/pdfs/Water%20Payments.jpg" style="width: 200px; height: 60px;" /></a></p>

<p><strong background-color:="" font-size:="" line-height:="" style="color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, " trebuchet=""><strong>Online Bill Pay</strong>&nbsp;-&nbsp;</strong><span style="line-height: 1.6em;">Customers of Mount Carmel&#39;s Water / Sewer Service &nbsp;may use our convenient online bill pay. Click on the picture link above.&nbsp;This will log into a secure server for online payments.</span></p>

<p><strong>Direct Debit -&nbsp;</strong>No more filling out checks and mailing, or worrying about whether the payment gets here on time. You can now have your utility bill payment deducted automatically from your bank account each month. If you are interested complete the <a href="/assets/city_files/pdfs/Automaticbankpay.pdf" target="_blank"><font color="#00a152">authorization form</font></a> and bring to City Hall at 219 N Market Street.</p>

<p><font face="Arial, Helvetica, sans-serif" size="2"><b>Through your bank -</b> When paying the utility statement through a bank&#39;s online bill pay service please verify that your account number matches your <strong>current </strong> statement. Payments made through an online bill pay service may be<strong> </strong> electronically transferred. Remember the payment must be received by the due date. The check must be mailed by the service to:&nbsp;219 N Market Street ; Attn: Utility Billing;&nbsp;Mount Carmel, Il&nbsp; 62863</font></p>

<p><strong><font face="Arial, Helvetica, sans-serif" size="2">In person - </font></strong><font face="Arial, Helvetica, sans-serif" size="2">Payments are accepted at the Assistance Desk, located in the lobby at City Hall. The office hours are 8 a.m. - 4:30 p.m. , Monday - Friday (closed all major holidays).</font></p>

<p><font face="Arial, Helvetica, sans-serif" size="2"><strong>Drop box - </strong>Payments can also be placed in the white drop box located in front of City Hall in the drive thru</font></p>

<p><font face="Arial, Helvetica, sans-serif" size="2"><b>By mail -</b> Please include the payment <strong>stub</strong> and send checks to 219 N Market Street, Mount Carmel, IL&nbsp; 62863.</font></p>

<p>The City of Mount Carmel provides water and sewer service throughout the city and other limited areas. Listed below are rate schedules for water and sewer usage.</p>

<h4>Water Rate</h4>
<small>Effective after 04/30/2011</small>

<table>
	<tbody>
		<tr>
			<td class="table_head">Usage</td>
			<td class="table_head">Rate</td>
		</tr>
		<tr>
			<td>Minimum (First 1,000 gallons)</td>
			<td>$7.09</td>
		</tr>
		<tr>
			<td>Next 99,000 gallons</td>
			<td>$4.71</td>
		</tr>
		<tr>
			<td>Next 150,000 gallons</td>
			<td>$4.22</td>
		</tr>
		<tr>
			<td>Next 500,000 gallons</td>
			<td>$3.41</td>
		</tr>
		<tr>
			<td>Next 500,000 gallons</td>
			<td>$2.74</td>
		</tr>
		<tr>
			<td>Over 1,250,000 gallons</td>
			<td>$1.87</td>
		</tr>
	</tbody>
</table>

<p><small>All rates are Per 1,000 gallons</small></p>
</div>

<div class="cushycms">
<h4>Water Tap Fees<br />
<br />
Inside Corporate Limits<br />
<span style="color: rgb(105, 105, 105);"><kbd><code><var>3/4&nbsp;Inch Tap&nbsp; $550.00,&nbsp;1 Inch Tap $750.00,&nbsp;2 Inch Tap $ 2,350.00, Tap Larger than 2 Inches will include additional costs</var></code></kbd></span><br />
<br />
Outside Corporate Limits<br />
<span style="color: rgb(105, 105, 105);"><samp>3/4 Inch Tap&nbsp; $650.00,&nbsp;1 Inch Tap $850.00,&nbsp;&nbsp; 2 Inch Tap $ 2,550.00,<br />
Tap Larger than 2 Inches will include additional costs</samp></span></h4>

<h4><br />
<br />
Sewer Rates Effective 04/20/2016</h4>

<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">
	<tbody>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Residential Rates</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Usage</td>
			<td>Rate</td>
		</tr>
		<tr>
			<td>Minimum (first 1000 Gallons water used)</td>
			<td>$11.95</td>
		</tr>
		<tr>
			<td>Per 1000 Gallons after first 1000</td>
			<td>$3.29</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Commercial Rates</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Usage</td>
			<td>Rate</td>
		</tr>
		<tr>
			<td>Minimum ( first 1,000 Gallons water Used)</td>
			<td>$11.95</td>
		</tr>
		<tr>
			<td>Per 1,000 Gallons after first 1,000</td>
			<td>$2.79</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<h4>&nbsp;</h4>

<p>&nbsp;</p>

<h4>Sewage Tap Fees</h4>

<p>Each user connecting to the sewer system is charged a sewer tap fee.</p>

<table>
	<tbody>
		<tr>
			<td>Inside Corporate Limits</td>
			<td>$350.00</td>
		</tr>
		<tr>
			<td>Outside Corporate Limits</td>
			<td>$500.00</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div id="garbage"><img alt="garbage" class="right cushycms" src="/assets/images/garbage.jpg" style="border: currentColor;" width="300" />
<div class="cushycms">
<h3>Garbage</h3>

<p>The City also provides garbage pick up for residential users both inside and outside the corporate limits.</p>

<table>
	<tbody>
		<tr>
			<td>Inside Corporate limits</td>
			<td>$13.00</td>
		</tr>
		<tr>
			<td>Outside Corporate limits</td>
			<td>$17.75</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div id="apps">
<div class="cushycms">
<h4>Application for service; and deposit.</h4>

<p><a href="/assets/city_files/pdfs/Water%20Application%20Rev%2004-04-13.pdf" target="_blank">Water Service Application</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/assets/city_files/pdfs/WATER_SERVICE_termination.pdf" target="_blank">Water Termination Form</a></p>

<p>Water and sewer service must be applied for in writing to the Water and Sewer department, located in the city hall building located at 219 N. Market Street. For customers not owning legal title to the property where service is requested, a security deposit is required.</p>

<p>(a) Residential property where the customer does not own legal title - One Hundred Dollars ($100.00);</p>

<p>(b) Commercial or Industrial uses - One Hundred Twenty-Five Dollars ($125.00).</p>

<p>When service is discontinued, the city shall refund the deposit after deducting therefrom any amounts owed by the user for unpaid water or sewer bills. (Ord. No. 114, 2-10-64; Ord. No. 368, x 1, 2-6-78; Ord. No. 388, x 1, 11-29-78; Ord. No. 448, x 1, 11-21-83,Ord. No. 368, x 2, 2-6-78; Ord. No. 448, x 2, 11-21-8; Ord. No. 913, x, 04/21/08)</p>
</div>

<div class="cushycms">
<h4>When billings are due and payable; penalties.</h4>

<p>All payments for water and sewerage services become due and payable as follows:<br />
All Cycles&nbsp; on the fifth (5th) day of each month<br />
<br />
If not paid by the&nbsp;fifth (5th) of the month, ten (10) percent will be added to the net amounts;</p>

<div class="cushycms">
<h4>Discontinuation Of Service For Nonpayment - Reconnection Charge</h4>
(a) If water and sewer service charges are not paid within seven ( 7 ) days following the due date, the customer&#39;s or user&#39;s account shall be deemed delinquent. In the event of delinquency, the city clerk shall terminate water service and the customer or user shall not again be entitled to water service until the water and sewer charges have been paid in full, together with a service reconnection charge of twenty-five dollars ($25.00).

<div class="cushycms">
<h4>Arrangement Procedure</h4>

<p>(a) All current utility charges must be paid in full by the due date on bill. (i.e.&nbsp; due by the 5th of the month)</p>

<p>(b) Customers who are unable to meet their financial obligation for utility service resulting from temporary circumstances; such as unemployment, death in family, extended illness, or abnormally high medical expenses; are encouraged to seek appropriate financial institution or social service agency assistance in order to meet the requirements of their utility charges for the current period in order to avoid the delinquency deadline and service termination/shut-off proceedings.</p>

<p>(c) Individuals must provide written evidence and the City must verify by the due date of the current billing month that arrangements have been made between the customer and a bonafide social service agency or financial institution to meet the complete financial obligation of the current and any delinquent utility charges.</p>

<p>(d) Upon receipt of the written evidence and verification of payment assistance by the Utility Billing Office</p>

<p>(e) No extension will be granted based on personal credit or promises to pay by individual utility customers.</p>

<p>(f) All payment assistance payment extensions must be placed in writing; signed by both the customer and&nbsp;Social Service Director and filed with Utility Billing Office.</p>

<p>(g) Failure of the social service agency or financial institution to remit payment within twenty calendar days will subject the customer to proceedings under the Disconnection for Non-Payment/Shut-Off.</p>

<p>(h) If the social service agency or financial institution cannot guarantee payment in full within the twenty-day time limit, written notice from the institution must accompany the application for payment assistance payment extension and explicitly state when payment will be remitted to the City.</p>

<p>(i) Utility services that have already been disconnected for non-payment are not eligible for payment extensions. All charges associated with said accounts must be paid in full in cash prior to service reestablishment.</p>

<p>(j) Continuing requests for payment arrangements will not be accepted. Once payment arrangement options as established above are exhausted, service termination for nonpayment/shut off procedures shall result.</p>
</div>
</div>
</div>
</div>


		</div>
	</div>
</section>
<? include 'includes/footer.php';?>