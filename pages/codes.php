<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2>Codes and Ordinances</h2>
<img alt="roofing" src="/assets/city_files/pdfs/1LEAHMAEWAY.jpg" style="float: right; width: 448px; height: 297px;" />
<div class="pdfs"><a href="/assets/white-pages/Mount_Carmel_Code_Enforcement_Brochure.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Code Enforcemnt Brochure</p>
</div>

<div class="pdfs"><a href="/assets/white-pages/Arc-Fault_Circuit_Interrupter.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Arc-Fault Circuit Interrupter</p>
</div>

<div class="pdfs"><a href="/assets/white-pages/Smoke_Alarm_Regulations.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Smoke Alarm Regulations</p>
</div>

<div class="pdfs"><a href="/assets/white-pages/Illinois_Roofing_Licensing_Act.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Illinois Roofers and Licensing Act</p>
</div>

<div class="pdfs">
<p><a href="/assets/white-pages/Illinois_Roofers_License_Exam.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a></p>

<p>Illinois Roofers License Exam</p>
</div>

<div class="pdfs"><a href="/assets/white-pages/Illinois_Architecture_Practice_Act.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Illinois Architecture Practice Act</p>
</div>

<div class="pdfs"><a href="/assets/city_files/pdfs/ILLINOIS%20ARCHITECTURE%20PRACTICE%20ACT%201989.pdf" target="_blank"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>Fence Ordinance</p>
</div>

<div class="pdfs"><a href="/assets/white-pages/Home%204/Permits/Flood%20Plain%20Variance.pdf"><img alt="pdf-icon" src="/assets/images/icons/pdf-icon.png" /></a>

<p>&nbsp;</p>
</div>

<p><strong><span style="color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; background-color: rgb(255, 255, 255);">TO REGISTER A COMPLAINT ABOUT A LICENSED ROOFER OR CONTRACTOR ROOFING WITHOUT A LICENSE FOLLOW THIS LINK:</span></strong></p>

<p><a href="https://www.idfpr.com/Admin/Filing/DPR/Complaint.asp" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; background-color: rgb(255, 255, 255);" target="_blank">ILLINOIS DEPARTMENT OF PROFESSIONAL REGULATION COMPLAINT INTAKE&nbsp;</a></p>

<p><a href="/assets/city_files/pdfs/Public%20Act%2091%20Home%20Repair%20and%20Remodeling%20Act.pdf" target="_blank">Public Act 91 Home Repair and Remodeling</a>&nbsp;</p>

<p><a href="/assets/city_files/pdfs/home%20repair%20know%20your%20consumer%20rights%20brochure.pdf" target="_blank">Home Repair Know Your Consumer Rights Brochure</a></p>

<p><a href="/assets/city_files/pdfs/Some%20Key%20Points%20Concerning%20the%20Roofing%20Act%20and%20Home%20Repair%20Act.pdf" target="_blank">Key Points Concerning Roofing Act and Remodeling Act</a></p>

<p><a href="/assets/city_files/pdfs/When%20do%20I%20need%20a%20building%20permit%20in%20Illinois.pdf" target="_blank">When Do I Need A Building Permit?</a></p>

<p><a href="/assets/city_files/pdfs/Plans%20Needed.pdf" target="_blank">Building Permit Requirements</a></p>

<p><a href="/assets/city_files/pdfs/Tips%20on%20How%20to%20Hire%20a%20Contractor.pdf" target="_blank">Tips on Hiring A Contractor</a></p>

<p>&nbsp;</p>

<p><a href="http://library.municode.com/index.aspx?clientId=14746" target="_blank">Full List of Codes &amp; Ordanances</a></p>

<p><a href="/assets/city_files/pdfs/MTCARMEL%20ZONING%20MAP.pdf" style="line-height: 1.6em;" target="_blank">Zoning Map</a></p>

<p><a href="https://www.energycodes.gov/software-and-web-tools" target="_blank">https://www.energycodes.gov/software-and-web-tools</a></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>The City of Mount Carmel has adopted the following Codes</strong></p>
&nbsp;

<div class="clear h_divider" style="margin-left: 40px;">International Property Maintenance Code (2006 Edition)</div>

<p class="clear h_divider" style="margin-left: 40px;">International Building Code (2006 Edition)</p>

<p class="clear h_divider" style="margin-left: 40px;">International Residential Code (2006 Edition)</p>

<p class="clear h_divider" style="margin-left: 40px;">NEC (2006 Edition)</p>

<p class="clear h_divider" style="margin-left: 40px;">International Mechanical Code (2006 Edition)</p>

<p class="clear h_divider" style="margin-left: 40px;">&nbsp;</p>

<p class="clear h_divider" style="margin-left: 40px;">International Energy Conservation Code</p>

<p class="clear h_divider" style="margin-left: 40px;">&nbsp;</p>

<p class="cushycms">City Inspector Michael Gidcumb is charged with the enforcement of all Mount Carmel city codes. His office is located in City Hall, 219 Market Street.</p>

<div class="cushycms">
<h3>City Inspector</h3>

<div class="contact">
<h4><i>Michael Gidcumb</i></h4>

<div class="address">&nbsp;</div>

<p>219 N. Market St.</p>

<div class="phone">&nbsp;</div>

<p>(618) 262-4822</p>

<div class="fax">&nbsp;</div>

<p>(618) 262-4802</p>

<div class="email">&nbsp;</div>

<p><a href="mailto:mgidcumb@cityofmtcarmel.com">mgidcumb@cityofmtcarmel.com</a></p>

<div class="clear">&nbsp;</div>
</div>
</div>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>