<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
		<h2>Real Estate in Mount Carmel, Illinois</h2>

	<div id="realestate">
		<p class="cushycms" id="OpeningParagraph">Mount Carmel offers an excellent selection of residential and business properties. These realtors will assist you in locating the property that best fits your unique needs.</p>

			<div class="cushycms" id="DHI">
				<h3><a href="http://www.dhirealty.com/" target="_blank">DHI Realty</a></h3>
				<img alt="DHI-EMBLEM" class="right" src="/assets/images/DHI-EMBLEM.gif" width="200" />
				<p>415 N. Market St.<br />
				Mount Carmel, IL 62863<br />
				Phone: (618) 263-8622<br />
				Fax: (618) 262-4900<br />
				dhirealty@gmail.com<br />
				<a href="http://www.dhirealty.com" target="_blank">http://www.dhirealty.com</a></p>
			</div>

			<div class="cushycms" id="Hocking">
				<h3><a href="http://hockingrealestate.net/" target="_blank">Hocking Real Estate</a></h3>
				<img alt="hocking_logo" class="right" src="/assets/images/hocking_logo.png" width="200" />
				<p>123 West 4th st Suite #2<br />
				Mount Carmel IL 62863<br />
				Phone : (618) 262-8690<br />
				Fax : (618) 262-8665<br />
				Cell : (618) 262-3685<br />
				<a href="http://hockingrealestate.net" target="_blank">http://hockingrealestate.net</a></p>
			</div>

			<div class="cushycms" id="Mundy">
				<h3><a href="http://MUNDYREALESTATE.COM" target="_blank">Mundy Real Estate </a></h3>
				<img alt="mundy_logo" class="right" src="/assets/images/mundy_logo.png" style="width: 298px; height: 100px;" />
				<p>400 Chestnut St.<br />
				Mount Carmel<br />
				Phone: (618) 263-3131<br />
				Fax: (618) 263-3606<br />
				<a href="http://mundyrealestate.com" target="_blank">http://mundyrealestate.com</a></p>
			</div>

			<div class="cushycms" id="Storckman">
				<h3><a href="http://storckmanrealty.com" target="_blank">Sara Storckman Realty </a></h3>
				<img alt="ssr_logo" class="right" src="/assets/images/ssr_logo.png" width="200" />
				<p>226 Walnut St.<br />
				Mount Carmel<br />
				Phone: (618) 262-7698<br />
				Fax: (618) 262-5332<br />
				<a href="http://storckmanrealty.com" target="_blank">http://storckmanrealty.com</a></p>
			</div>

		</div>
	</div>
</div>

</section>
<? include 'includes/footer.php';?>