<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2>Education in Mount Carmel, Illinois</h2>
			<img alt="Middle School" class="right cushycms" src="/assets/city_files/100_0172.jpg" width="300" />
			<p class="cushycms">As a community, we believe that education is an ongoing, life long process. The education provided in our schools is student centered, with each encouraged to build a knowledge base that transfers to life situations. Every student is given the opportunity to develop themselves as self-fulfilled individuals in order to become productive members of their community, as well as society in general. An extensive list of activities is offered to students including; National Honor Society, Student Counsel, Music, Theater and the Arts, Scholastic Bowl, Clubs, a wide array of athletics and much more.</p>

			<div class="clear">&nbsp;</div>
			<img alt="Saint Mary's Catholic School" class="left cushycms" src="/assets/city_files/100_0114.jpg" width="300" />
			<p class="cushycms">Students enrolled in <a href="http://www.wabash348.com/" target="_blank">Wabash Community Unit School District #348 </a>may attend <a href="http://www.south.wabash348.com/" target="_blank">South Elementary School </a>(K-2), <a href="http://www.nice.wabash348.com/" target="_blank">North Intermediate Center of Education</a> (3-5), <a href="http://www.mcms.wabash348.com/" target="_blank">Mount Carmel Middle School </a>(6-8), and<a href="/www.mchs.wabash348.com/" target="_blank"> Mount Carmel High School </a>(9-12). For younger children, Day Care centers and Pre-Schools offer a range of services and schedules. Residents interested in parochial school may choose <a href="http://smsrockets.com/" target="_blank">St. Mary&#39;s Catholic School </a>with its Pre-K through 8th Grade programming.</p>

			<div class="clear">&nbsp;</div>
			<img alt="WVC" class="right cushycms" src="/assets/city_files/WVC%20photo.bmp" width="300" />
			<p class="cushycms">Mount Carmel is also proud to be home to <a href="http://www.iecc.edu/wvc/" target="_blank">Wabash Valley College</a>, a member of the Illinois Eastern Community College District, where students of all ages will find programs designed to provide both the path to academic excellence and practical knowledge.</p>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>