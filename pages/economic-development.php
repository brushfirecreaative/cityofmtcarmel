<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 class="cushycms" id="PageHeading">Economic Development</h2>

<p><a href="http://mcaea.com/" target="_blank"><img alt="" src="/assets/city_files/pdfs/area%20economic%20alliance.png" style="width: 216px; height: 135px;" /></a><a href="http://mcaea.com/" target="_blank">Executive Director Ben Ross</a></p>
<img alt="Blueprints" class="right cushycms" src="/assets/images/economic_development.jpg" width="400" />
<h4 class="cushycms">Business Infrastructure Assistance Program</h4>

<p class="cushycms">The City of Mount Carmel can supply TIF funds to industrial firms located or expanding in a TIF to cover up to 50 % of the cost of approved infrastructure improvements. These improvements can involve all elements of public way infrastructure. Eligible projects include but are not limited to: light pole and fire hydrant relocation; hazardous vaulted sidewalk elimination; traffic signalization; water and sewer line improvements; utility removal and relocation; sidewalk, curb, gutter and street improvements; and landscaping. Projects are based on individual property development agreement with the city based on project size and total employment.<br />
<br />
<br />
<br />
Enterprise Zone Map<a href="/assets/minutes/EPrize%20full%20view.jpg" target="_blank">&nbsp;Enterprise Zone </a></p>

<h4 class="cushycms">&nbsp;</h4>

<h4 class="cushycms">Revolving Loan Fund</h4>

<p class="cushycms">A low-interest loan program that is based on job creation with a lending rate of 10,000 per full-time job created or retained</p>

<h4 class="cushycms">Training</h4>

<p class="cushycms">We bring training experts from the Illinois Eastern Community College District to your company to assess your training needs, customize a curriculum and set up training at your facility, depending on your needs. Training ranges from OSHA to ESL to supervisory and industrial skills.</p>

<ul class="cushycms">
	<li>
	<ul>
		<li><b>TIF:</b> Companies located within, expanding or relocating to an eligible TIF district are eligible for a training reimbursement of up to 100 percent of an employer&#39;s cost of developing or purchasing customized training programs.</li>
		<li><b>Employment Training Investment Program (ETIP):</b> Funded through the Illinois Department of Commerce and Economic Opportunity (DCEO) provides a rebate of up to 50 % of training expenses for full-time workers.</li>
	</ul>
	</li>
</ul>

<h4 class="cushycms">Tax Credits</h4>

<ul class="cushycms">
	<li>
	<ul>
		<li><b>Illinois Department of Employment Security (IDES)</b> administers State of Illinois tax credits including Work Opportunity and Welfare-to-Work tax credits, tax credits for ex-felons, veterans, youth and others.</li>
		<li><b>Enterprise Zone Job tax credits:</b> The enterprise zone tax credit 35 ILCS 5/201 offers employers a tax credit on their Illinois Income taxes for hiring individuals who are certified as economically disadvantaged or as dislocated workers working a minimum of 180 consecutive days for 30 hours or more per week.</li>
		<li><b>Enterprise Zone Property Tax Abatement:</b> 5 year 100% property tax abatement on improved property in Enterprise Zone.</li>
		<li><b>Enterprise Zone State Sales Tax Deduction</b> on permanent building materials &amp; machinery in Enterprise Zone.</li>
		<li><b>Enterprise Zone Income Tax Deduction</b> for Financial Institutions-Financial Institutions are eligible for a special deduction from their Illinois Corporate Income Tax Return. Such institutions may deduct from their taxable income an amount equal to the interest received from a loan for development in an Enterprise Zone.</li>
	</ul>
	</li>
</ul>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>