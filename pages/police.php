<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<table border="0" cellpadding="0" cellspacing="0" style="width: 700px;">
	<tbody>
		<tr>
			<td colspan="4" style="width: 482px;">
			<h1 style="margin-left: 400px;"><strong>Mt. Carmel Police Department</strong></h1>

			<p style="margin-left: 440px;">120 E 4th Street, Mt. Carmel, Illinois 62863</p>

			<hr size="1" style="width: 1418px;" />
			<p style="margin-left: 40px;"><img alt="" src="/assets/city_files/pdfs/DETECTIVE%20SERGEANT%20MC8.jpg" style="width: 184px; height: 257px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/SQUAD%20CAR%20WEB.jpg" style="width: 472px; height: 346px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/MC4%20CHIEF%20OF%20POLICE.jpg" style="width: 184px; height: 257px;" /></p>

			<h2>&nbsp;</h2>

			<h1 style="margin-left: 480px;"><strong>Contact Information</strong></h1>

			<p>Email: <a href="mailto:jlockhart@wabash911.net">John Lockhart Chief of Police</a>&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp; Emergency Line: 911&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;Crime Tip Line: (618)262-4258&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;Police Dispatch (618)262-4114 or (618)262-4115</p>

			<p style="margin-left: 200px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;*&nbsp;&nbsp; Administrative Office (618)262-4433</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>Mission Statement</strong></h1>

			<p>The men and women of the Mt. Carmel Police Department are dedicated to providing the citizens of our community with quality professional police</p>

			<p>protection, done so with honesty integrity and a sense of fairness. The Mt. Carmel Police Department and it&rsquo;s employees value service professionalism</p>

			<p>and compassion in serving our community.</p>

			<p>&nbsp;</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>Administration</strong></h1>

			<p>The Mt.Carmel Police Department consist of 21 sworn and civilian employees which includes a full-time Chief and Administrative Assistant. The Chief is</p>

			<p>responsible for the day-to-day operations of the department.</p>

			<p>&nbsp;</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>Investigations</strong></h1>

			<p><img alt="" src="/assets/city_files/pdfs/DETECTIVE%20SERGEANT%20MC8.jpg" style="width: 184px; height: 257px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/ryans%20car.jpg" style="width: 456px; height: 342px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/DETECTIVE%20SERGEANT%20MC8.jpg" style="width: 184px; height: 257px;" /></p>

			<p>&nbsp;</p>

			<p>The Mt. Carmel Police Department Investigations is commanded by a Detective Sergeant. This department is responsible for crime scene investigations,</p>

			<p>criminal investigations, and drug related criminal activity</p>

			<p>&nbsp;</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>Patrol</strong></h1>

			<p><img alt="" src="/assets/city_files/pdfs/SERGEANT%20MC13.jpg" style="width: 184px; height: 253px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/PATROL%20STOP.jpg" style="width: 527px; height: 280px;" />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/SERGEANT%20MC14.jpg" style="width: 180px; height: 241px;" /></p>

			<p>The patrol division is divided into three regular shifts and one relief shift, each commanded by a Shift Sergeant. Patrol provides 24 hour</p>

			<p>7 day a week protection.</p>

			<p>&nbsp;</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>K-9 Unit</strong></h1>

			<p style="margin-left: 40px;"><img alt="" src="/assets/city_files/pdfs/MC%2011%20PATROLMAN.jpg" style="width: 176px; height: 225px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/k9%20unit.jpg" style="width: 472px; height: 354px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/MC%2011%20PATROLMAN.jpg" style="width: 166px; height: 229px;" /></p>

			<p style="margin-left: 360px;">&nbsp;</p>

			<p>The Mt. Carmel Police Department has had a K-9 division since 1995. This unit is an integral part of our operation assisting in drug detection,</p>

			<p>tracking and apprehension.</p>

			<p>&nbsp;</p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 400px;"><strong>Juvenile Officer Division</strong></h1>

			<p><img alt="" src="/assets/city_files/pdfs/MC%2016%20PATROLMAN.jpg" style="width: 176px; height: 245px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="/assets/city_files/pdfs/sro%20Greg_0096.jpg" style="width: 514px; height: 223px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt="" src="/assets/city_files/pdfs/MC%207%20PATROLMAN.jpg" style="width: 167px; height: 245px;" /></p>

			<p style="margin-left: 360px;">&nbsp;</p>

			<p>The Mt. Carmel Police Department employees 3 juvenile officers and a school resource officer in conjunction with Wabash Community District 348.</p>

			<p style="margin-left: 360px;">&nbsp;</p>

			<p><strong>U.S. Immigration and Customs Enforcement</strong></p>

			<p>U.S. Immigration and Customs Enforcement (ICE), Homeland Security Investigations (HSI),</p>

			<p>is committed to protecting the public and ensuring that the rights of victims are carefully observed.</p>

			<p>HSI administers the Victim Notification Program which allows eligible victims and</p>

			<p>witnesses to obtain reliable and timely information regarding a criminal alien&#39;s release from custody.</p>

			<p>For more information follow this link:<a href="http://www.ice.gov/victim-notification/" target="_blank"> http://www.ice.gov/victim-notification/</a></p>

			<hr size="1" style="width: 1418px;" />
			<h1 style="margin-left: 480px;"><strong>Related Links</strong></h1>

			<p>&nbsp;</p>

			<p><a href="http://www.isp.state.il.us/" target="_blank">Illinois State Police</a><br />
			<a href="http://www.isp.state.il.us/sor/" target="_blank">Illinois State Police Sex Offender Website</a><br />
			<a href="http://www.state.il.us/dcfs/index.shtml" target="_blank">Illinois Department of Children and Family Services</a><br />
			<a href="http://www.amw.com/" target="_blank">America&#39;s Most Wanted</a><br />
			<a href="http://www.dare-america.com/" target="_blank">D.A.R.E.</a><br />
			<a href="http://www.charactercounts.org/" target="_blank">Character Counts</a></p>

			<p><a href="http://www.consumeraffairs.com/privacy/index.html" target="_blank">Identitytheftprotection</a></p>
			</td>
		</tr>
		<tr>
			<td rowspan="5">
			<p align="center">&nbsp;</p>
			</td>
			<td style="width: 164px;">
			<p align="center">&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 164px;">
			<p>&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width: 164px;">
			<p>&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
			<td style="width: 82px;">
			<p align="center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="width: 328px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="width: 328px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<h1>&nbsp;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<h1>&nbsp;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<h1>&nbsp;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<h1>&nbsp;</h1>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<div align="center">&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<div align="center">
			<div align="center">
			<div align="center">&nbsp;</div>
			</div>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<div align="center">&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 482px;">
			<p>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>