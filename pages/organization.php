<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2 id="PageHeading">&nbsp;</h2>

<p>Mount Carmel City Government Organization</p>

<div id="org">
<div id="mayor">
<h3>Mayor of the City of Mount Carmel</h3>

<p><img alt="" src="/assets/city_files/pdfs/william%20hudson.jpg" style="width: 150px; height: 209px;" /></p>

<h4><a href="mailto:council@cityofmtcarmel.com?subject=Mayor%20%26%20City%20Council">William C. Hudson</a></h4>
</div>

<div class="second_tier">
<div>
<h5>City Clerk and Administrative Assistant</h5>

<p><img alt="" src="/assets/city_files/pdfs/Rudy%201.jpg" style="width: 130px; height: 119px;" /></p>

<p><a href="mailto:rudyw@cityofmtcarmel.com?subject=City%20Clerk">Rudy L.Witsman</a></p>
</div>

<div>
<h5>City Treasuer</h5>

<p><img alt="" src="/assets/city_files/pdfs/georgia%20vaught.JPG" style="width: 130px; height: 119px;" /></p>

<p>Georgia Vaught</p>
</div>

<div>
<h5>Police Chief</h5>

<p><img alt="" src="/assets/city_files/pdfs/Lockhart%202%20DSC00876.jpg" style="width: 130px; height: 119px;" /></p>

<p>John Lockhart</p>
</div>

<div>
<h5>Airport Manager</h5>

<p><img alt="" src="/assets/city_files/pdfs/ken%20wood.jpg" style="width: 130px; height: 119px;" /></p>

<p>Ken Wood</p>
</div>

<div>
<h5>City Inspector</h5>

<p><img alt="" src="/assets/city_files/pdfs/michael%20gidcumb.jpg" style="width: 130px; height: 101px;" /></p>

<p>Mike Gidcumb</p>
</div>
</div>

<div class="clear">&nbsp;</div>
&nbsp;

<div class="h_divider">&nbsp;</div>

<div class="council_member">
<h3>Commissioner of Fire, City Hall, Garbage, Civil Defense and Health &amp; Safety</h3>

<p><img alt="" src="/assets/city_files/pdfs/rod.jpg" style="width: 130px; height: 119px;" /></p>

<h4><a href="mailto:council@cityofmtcarmel.com?subject=Mayor%20%26%20City%20Council">Rod Rodriguez</a></h4>

<div class="second_tier">
<h5>Fire Chief</h5>

<p><img alt="" src="/assets/city_files/pdfs/francis%20speth.jpg" style="width: 148px; height: 135px;" /></p>

<p>Francis Speth</p>
</div>
</div>

<div class="clear">&nbsp;</div>
&nbsp;

<div class="h_divider">&nbsp;</div>

<div class="council_member">
<h3>Commissioner of Finance and Senior Citizen</h3>

<p><img alt="" src="/assets/city_files/pdfs/Joe%20Judge.jpg" style="width: 130px; height: 99px;" /></p>

<h4><a href="mailto:council@cityofmtcarmel.com?subject=Mayor%20%26%20City%20Council">Joe Judge</a></h4>
</div>

<div class="second_tier">
<div>
<h5>City Comptroller</h5>

<p><img alt="" src="/assets/city_files/pdfs/mick%20mollenhauer.jpg" style="width: 106px; height: 119px;" /></p>

<p>Mick Mollenhauer</p>
</div>

<div>
<h5>Senior Citizens Director</h5>

<p><img alt="" src="/assets/city_files/pdfs/TRACY.JPG" style="width: 130px; height: 119px;" /></p>

<p>Tracy Dunkel</p>
</div>
</div>

<div class="clear">&nbsp;</div>
&nbsp;

<div class="h_divider">&nbsp;</div>

<div class="council_member">
<h3>Commissioner of Water and Sewage</h3>

<p><img alt="" src="/assets/city_files/pdfs/dulgar.jpg" style="width: 130px; height: 99px;" /></p>

<h4><a href="mailto:council@cityofmtcarmel.com?subject=Mayor%20%26%20City%20Council">Justin Dulgar</a></h4>
</div>

<div class="second_tier">
<div>
<h5>Water Plant Director</h5>

<p><img alt="" src="/assets/city_files/pdfs/keith%20reed.jpg" style="width: 130px; height: 112px;" /></p>

<p>Keith Reed</p>
</div>

<div>
<h5>Sewage Plant Operator</h5>

<p><img alt="" src="/assets/city_files/pdfs/JOSH%20PEACH%20sp2.jpg" style="width: 119px; height: 130px;" /></p>

<p>Josh Peach</p>
</div>

<div>
<h5>Water &amp; Sewage Maintenance Operator</h5>

<p><img alt="" src="/assets/city_files/pdfs/bill%20reed.JPG" style="width: 130px; height: 119px;" /></p>

<p>Bill Reed</p>
</div>
</div>

<div class="clear">&nbsp;</div>
&nbsp;

<div class="h_divider">&nbsp;</div>

<div class="council_member">
<h3>Commissioner of Streets, Cemetery &amp; Parks</h3>

<p><img alt="" src="/assets/city_files/pdfs/IKEMIRE.jpg" style="width: 126px; height: 119px;" /></p>

<h4><a href="mailto:council@cityofmtcarmel.com?subject=Mayor%20%26%20City%20Council">Eric Ikemire&nbsp;</a></h4>
</div>

<div class="second_tier">
<div>
<h5>Superintendent of Streets, Cemetery &amp; Parks</h5>

<p><img alt="" src="/assets/city_files/pdfs/dave%20easter%201.jpg" style="width: 113px; height: 130px;" /></p>

<p>Dave Easter</p>
</div>
</div>
</div>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>