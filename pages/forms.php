<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h2>Applications</h2>
&nbsp;

<div class="fivecol">
<table cellpadding="0" cellspacing="0" style="width: 1300px; height: 611px;">
	<tbody>
		<tr class="pdfs">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="pdfs">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;"><a href="/assets/city_files/pdfs/BIRTH_SEARCH.pdf" target="_blank">Birth Record Search </a></td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Mount%20Carmel%20Public%20Utility%20Easement%20%20Sign%20off%20Form.pdf" target="_blank">Building Easment</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/BUILDINGPERMIT_Revised11-25-14.pdf" target="_blank">Building Permit </a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/COD_LICENSE_APPLICATION.pdf" target="_blank">Coin Operated Device </a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Community%20Room%20Rental.pdf" target="_blank">Community Room Rental</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/minutes/permits/ContractorRegistration.pdf" target="_blank">Contractor Registration </a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/DEATH_SEARCH.pdf" target="_blank">Death Record Search</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Automaticbankpay.pdf" target="_blank">Direct Debit </a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/minutes/permits/DemolitionOrdinance.pdf" target="_blank">Demolition Ordinance</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/Demo_Application_A.pdf" target="_blank">Demo Permit Application A</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/Demo_Application_B.pdf" target="_blank">Demo Permit Application B</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="http://www.fema.gov/pdf/nfip/elvcert.pdf" target="_blank">Elevation Certificate M </a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Employment%20application.pdf" target="_blank">Employment Application</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Fence%20Application%20City%20of%20Mt.%20Carmel.pdf" target="_blank">Fence Application</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/Flood_Plain_Dev_Permit.pdf" target="_blank">Flood Plain Development</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/Fund%20Raising%20Activity%20on%20Public%20Streets.pdf" target="_blank">Fund-Raising Public Streets</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/LIQUOR.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Liquor License</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p><a href="/assets/minutes/permits/SIGNS%20APPENDIX%20H.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Sign Application</a></p>
			</td>
			<td style="white-space: nowrap;">
			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/SUBDIVISION_APPLICATION_TYPE_B.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Subdivision Application B</a></p>
			</td>
			<td>
			<p><a href="/assets/white-pages/SPECIAL_USE_PERMIT_APPLICATION.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Special Use Permit</a></p>
			</td>
			<td>
			<p>&nbsp;</p>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/city_files/pdfs/special%20event.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; background-color: rgb(255, 255, 255);" target="_blank">Special Event</a></p>
			</td>
			<td>
			<p><a href="/assets/city_files/pdfs/Solicitor%20License%20Application.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Solicitor/Peddler App</a></p>
			</td>
			<td>
			<p><a href="/assets/white-pages/SWIMMING_POOL_PERMIT_APPLICATION.pdf" style="color: rgb(7, 130, 193); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.7999992370605px; white-space: nowrap; background-color: rgb(255, 255, 255);" target="_blank">Swimming Pool Application</a></p>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/REQUEST_VARIANCE_APPLICATION.pdf" target="_blank">Variance Application</a></p>
			</td>
			<td>
			<p><a href="/assets/images/permits_23_83095044.pdf" target="_blank">Video Gaming</a><a href="/assets/white-pages/Water_Tap_Application_Outside_City.pdf"> </a></p>
			</td>
			<td>
			<p><a href="/assets/white_pages/pdfs/Water%20Application%20Rev%2004-04-13.pdf" target="_blank">Water Service </a></p>
			</td>
			<td>
			<p style="margin-left: 40px;">&nbsp;</p>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;">
			<p><a href="/assets/white-pages/Water_Tap_City_Limits_Application.pdf" target="_blank">Water Tap City Limits </a></p>
			</td>
			<td>
			<p><a href="/assets/white-pages/Water_Tap_Application_Outside_City.pdf" target="_blank">Water Tap Outside City</a></p>
			</td>
			<td><a href="/assets/white-pages/WATER_SERVICE_termination.pdf" target="_blank">Water Termination</a></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;"><a href="/assets/city_files/pdfs/MTCARMEL%20ZONING%20MAP.pdf" target="_blank">Zoning Map</a></td>
			<td><a href="/assets/city_files/pdfs/EnergyCodeChecklist_Mt.Carmel%202014.pdf" target="_blank">Energy Code checklis</a><a href="/assets/city_files/pdfs/EnergyCodeChecklist_Mt.Carmel-3.pdf" target="_blank">t</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<h4>&nbsp;</h4>
</div>

<div class="clear h_trider"><img alt="building plans" class="right" src="/assets/images/building-plans.jpg" />
<div class="cushycms">
<h3>When is a building permit required?</h3>

<ol>
	<li>Construction of any building or other structure with a total construction cost in excess of $100.00.</li>
	<li>Construction of an addition to any building or structure.</li>
	<li>Demolition of a building or structure.</li>
	<li>A change in the occupancy of a building that effects fire egress and exit requirements or other occupancy restrictions.</li>
	<li>The installation or alteration of any equipment regulated by this code.</li>
	<li>Any change to a roof line, e.g. from a flat roof to a gabled or hip roof also normal shingle replacement.</li>
</ol>

<p>Electrical permits, plumbing permits and mechanical permits are considered separately.<br />
A building permit is NOT required for any building maintenance, such as siding, painting, replacement of doors and windows in existing openings. UNLESS, it would cause a change in occupancy requirements or affect required fire egress and exits etc.</p>
</div>

<div class="cushycms">
<h3>What is the procedure for Permit Application?</h3>

<ol>
	<li><a href="/assets/white-pages/Building_Permit_City_Of_Mt_Carmel.pdf" target="_blank">Fill out the Application for Building Permit</a> and Plan Review, providing all information required for the project. For one and two family dwellings the applicant must also complete the One &amp; Two Family Plan Review form.

	<ul>
		<li>For structures to be constructed within a subdivision, the applicant should be aware of any restrictive covenants that may apply. Generally, covenants are recorded at the Office of the County Clerk.</li>
	</ul>
	</li>
	<li>Any application submitted for the construction of a building or structure to be used by the public MUST include a set of plans for such on which an architect or structural engineer licensed by the State of Illinois to perform such work has signed and affixed his seal thereon.</li>
	<li>The site plan must indicate all set-back distances and dimensions (check for conformance with zoning regulations). All set-backs are measured from the property line and NOT from the curb or street center.</li>
	<li>A requirement for the issuance of a demolition permit is for the applicant to NOTIFY JULIE for a dig number. This will assure that utilities have been properly disconnected.</li>
</ol>
</div>
</div>
		</div>
	</div>
</section>
<? include 'includes/footer.php';?>