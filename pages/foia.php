<? include 'includes/header.php'; ?>
<section id="main_content">
	<div class="row">
		<div id="main_wrap">
			<h1>Freedom of Information Act</h1>

<h1>Mayor, City Council, Executive Staff Building Locations</h1>

<div class="cushycms">
<h3><strong>Freedom of Information Officer</strong></h3>

<div class="contact">
<h4><i>Rudy L. Witsman</i></h4>

<div class="address">&nbsp;</div>

<p>219 N. Market St.</p>

<div class="phone">&nbsp;</div>

<p>(618) 262-4822</p>

<div class="fax">&nbsp;</div>

<p>(618) 262-4208</p>

<div class="email">&nbsp;</div>

<p><a href="mailto:rudyw@cityofmtcarmel.com?subject=Freedom%20of%20Information%20Request">rudyw@cityofmtcarmel.com</a></p>
</div>

<p>Mount Carmel has a strong mayor-council form of government. The Mayor chairs all meetings of the City Council. The Mayor and four members of the City Council work part-time, holding budget and policy-setting authority for the City.</p>

<p>The Mayor and City Council members are elected at large by popular vote for a term of four years. City Council members are elected to a specific Council seat, but the seats are not determined geographically. City elections are held in odd numbered years every fourth year (e.g. 2007, 2011, 2015). The next city election will be held on the first Tuesday in April 2015.</p>

<p>The City Council meets formally every other Monday at 5:00 p.m. The Mayor is responsible for the Police Department as well as the public affairs of the City. Each Commissioner is responsible for their respective departments. The four departments are: Finances and Senior Citizens; Streets, Cemeteries and Parks; Fire Health and Safety, City Hall, Garbage and Civil Defense; and Water and Sewer.</p>

<p>The City Clerk, City Treasurer, Comptroller and all Department Heads are appointed with the consent of the Mayor and City Council.</p>

<p>The City Clerk&#39;s office provides centralized direction and leadership for the effective administration and operations of all municipal services for the City of Mount Carmel as directed by the City Council. It also serves as the focal point for the management of the City staff agencies. The City Clerk&#39;s Office prepares and submits to the City Council a balanced plan of municipal services in adherence with the policy goals and objectives established by the City Council while employing such managerial techniques as needed to assure efficient and effective utilization of the City&#39;s resources. The City Clerk is responsible for the day to day operation of the City and also serves as the Freedom of Information Officer.</p>

<p>There are currently 69 full-time employees and 88 part-time and seasonal workers. The operating budget for fiscal year 2010, which runs from May 1, 2009 to April 30, 2010, is $8,348,165</p>
</div>

<div class="cushycms">
<h2>FREEDOM OF INFORMATION REQUEST</h2>

<h4>What types of materials are available without filing a FOIA request?</h4>

<p><a href="/city_council_meetings.html">Council Minutes</a> (also available on our web site. <a href="/city_council_meetings.html">Liquor Control Minutes</a> <a href="/includes/codes.html">Ordinances</a> <a href="/city_council_meetings.html">Zoning Board Minutes</a>)</p>

<h4>What types of materials are available through a FOIA request?</h4>

<ul>
	<li>Warrants</li>
	<li>Contracts</li>
	<li>Agreements</li>
	<li>Employee List</li>
	<li>Employee Salaries</li>
	<li>Pension Information</li>
</ul>

<p>This is not an all inclusive list of available materials.</p>

<p>If you need copies of records from the City of Mount Carmel, please obtain and complete a Freedom of Information Request Form from the City Clerk&#39;s Office or follow the link below. When completed return your Freedom of Information Request to Rudy L. Witsman at City Hall.</p>

<h4><a href="/assetswhite-pages/CITY%20OF%20MOUNT%20CARMEL%20FOIA%20REQUEST.pdf">FOIA Request Form</a></h4>

<p><strong>Records are housed at the following locations:</strong></p>

<ul>
	<li>City Hall/City Clerk&#39;s Office - 219 Market St.</li>
	<li>Fire Department - 830 Walnut St.</li>
	<li>Police Department - 110 E. 4th St.</li>
</ul>

<p>Records and information will be made available for inspection and copying unless the records or information are exempt under the Freedom of Information Act (list of exemptions available upon request in the City Clerk&#39;s Office). Records can be given to you in an electronic format if the requested format is feasible for the City. If the requested format is not feasible, then, at the requester&rsquo;s option, the City will provide the records in the electronic format in which they are kept or in paper form.</p>

<p>Each request will be complied with or denied within five business days after its receipt. The five day period may be extended by an additional five business days if the City can demonstrate that certain conditions exist, such as a large amount of records or the need for extensive review.</p>

<p>If the City denies a records request, then the requester may seek review of that denial either through the Illinois Attorney General&#39;s Public Access Counselor or through the court. The Public Access Counselor&#39;s phone number is: 312-814-5526 or 877-299-3642, the fax number is: 217-782-1396, and the e-mail address is:<br />
<a href="mailto:publicaccess@atg.state.il.us">publicaccess@atg.state.il.us</a></p>

<h5><strong>Charge per copy:</strong></h5>

<p>First 50 pages copied are free, then .15 per sheet for black and white, letter or legal sized copies.</p>

<h5>Charge per Electronic format:</h5>

<p>The cost of materials will be charged.</p>

<h5><strong>Charge for Certification:</strong></h5>

<p>$1.00 (excluding Police Department and Library)</p>
</div>

<div class="cushycms">
<h2>Building Locations</h2>

<h3>City Hall</h3>

<div class="address">&nbsp;</div>

<p>219 Market Street</p>

<p><strong><i>Includes Following Offices:</i></strong></p>

<ul>
	<li>Mayor</li>
	<li>City Clerk</li>
	<li>Treasurer</li>
	<li>Building Inspector</li>
	<li>Community Betterment</li>
	<li>Water Department Billing</li>
</ul>

<h3><strong>Fire Station</strong></h3>

<div class="address">&nbsp;</div>

<p>830 Walnut St.</p>

<h3>Water &amp; Sewage Maintenance</h3>

<div class="address">&nbsp;</div>

<p>811 Poplar St.</p>

<h3><strong>Police Station</strong></h3>

<div class="address">&nbsp;</div>

<p>110 E. 4<sup>th</sup> St.</p>

<h3><strong>Public Library</strong></h3>

<div class="address">&nbsp;</div>

<p>727 N. Mulberry St.</p>

<h3><strong>Sewage Treatment Plant</strong></h3>

<div class="address">&nbsp;</div>

<p>125 S. Division St.</p>

<h3><strong>Water Treatment Plant</strong></h3>

<div class="address">&nbsp;</div>

<p>119 S. Cherry St.</p>
</div>

<div class="cushycms">
<h2>CITY OF MOUNT CARMEL BOARDS AND COMMISSIONS</h2>

<h3><strong>Airport</strong></h3>

<p>Jim Henning, Kenneth Kline, Jim Litherland, Bill Hudson, Bob Mundy Jr., Charles Sanders, Jim Wilderman</p>

<h3><strong>Area Agency on Aging</strong></h3>

<p>Ed Bennett, Chester Miles</p>

<h3><strong>Fire and Police</strong></h3>

<p>Ed Hicks, Jack Rother, Shad White</p>

<h3><strong>Fire Pension</strong></h3>

<p>Rudy Witsman, Terry Mc Guire, Aaron Brown, Glen Markle,&nbsp;Mark Seaton</p>

<h3><strong>Greater Wabash Regional Planning Commission</strong></h3>

<p>Joseph Judge</p>

<h3><strong>Library</strong></h3>

<p>Sharon Purvis, Scott Witsman, Bill Easton., Darlene Weir, Rian Waterbury, Nancy Price, Charles Randolph, Verlin Snow,&nbsp;Lisa Fischer</p>

<h3><strong>Police Pension</strong></h3>

<p>Don Price, Jared Price, Ryan Turner, Merle Weems, Rudy Witsman</p>

<h3><strong>Revolving Loan Fund</strong></h3>

<p>Rob Coleman, Joe Judge, Bill Hudson, Rudy Witsman, C. Michael Witters</p>

<h3><strong>TIF</strong></h3>

<p>Robert Bethards, Bill Hudson</p>

<h3><strong>Zoning</strong></h3>

<p>Harold Bailey, Tony Bowles, Patty Bramlet, Tim Raibley, Bill Mabry, Bob McGregor, Jeff Parker</p>

<p><em>The City of Mount Carmel &bull; 219 N. Market St. &bull; Mount Carmel, IL 62863 &bull; (618) 262-4822</em></p>
</div>

		</div>
	</div>
</section>
<? include 'includes/footer.php';?>